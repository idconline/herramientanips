////
/// Copyright (c) 2016 Saúl Piña <sauljabin@gmail.com>.
/// 
/// This file is part of xmlrpcwsc.
/// 
/// xmlrpcwsc is free software: you can redistribute it and/or modify
/// it under the terms of the GNU Lesser General Public License as published by
/// the Free Software Foundation, either version 3 of the License, or
/// (at your option) any later version.
/// 
/// xmlrpcwsc is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU Lesser General Public License for more details.
/// 
/// You should have received a copy of the GNU Lesser General Public License
/// along with xmlrpcwsc.  If not, see <http://www.gnu.org/licenses/>.
////

using System;
using XmlRpc;
using System.Xml;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Linq;
using ResponseModels;


namespace sandbox {

    /// <summary>
    /// This class, test the xmlrpc component on odoo 9
    /// </summary>
   public class OperationSand {
        //PRUEBAS
        public static string Url = "http://dev-idcerp.mx/xmlrpc/2", db = "odooDevTest", pass = "temporal01", user = "saul.luciano@idconline.mx";

        

        public static void TestRequestXml() {
            XmlRpcRequest request = new XmlRpcRequest("version");
            request.AddParam(false);
            request.AddParam(3);
            request.AddParam(4.9);
            request.AddParam(DateTime.Now);
            request.AddParam(DateTime.UtcNow);
            request.AddParam(Encoding.UTF8.GetBytes("hello"));

            Dictionary<string, object> dictest = new Dictionary<string, object>();
            dictest.Add("hello", "hello");
            // request.AddParam(dictest);

            List<object> listtest = new List<object>();
            listtest.Add(3);
            listtest.Add("hello");
            listtest.Add(dictest);
            request.AddParam(listtest);

            XmlDocument xmlRequest = RequestFactory.BuildRequest(request);

            xmlRequest.Save(Console.Out);

            XmlRpcClient client = new XmlRpcClient();
            client.AppName = "Test";
            Console.WriteLine("\n");
            Console.WriteLine(client.GetUserAgent());
        }

        public static void TestReadVersion() {
            XmlRpcClient client = new XmlRpcClient();
            client.Url = Url;
            client.Path = "common";

            XmlRpcResponse response = client.Execute("version");

            Console.WriteLine("version");
            Console.WriteLine("REQUEST: ");
            client.WriteRequest(Console.Out);

            Console.WriteLine();
            Console.WriteLine();
            Console.WriteLine("RESPONSE: ");
            client.WriteResponse(Console.Out);

            Console.WriteLine();
            Console.WriteLine();
            if (response.IsFault()) {
                Console.WriteLine(response.GetFaultString());
            } else {
                Console.WriteLine(response.GetString());
            }
        }

        public static void PruebaComentarPedido(string pedido)
        {
            XmlRpcClient client = new XmlRpcClient();
            client.Url = Url;
            client.Path = "common";
            XmlRpcRequest requestLogin = new XmlRpcRequest("authenticate");
            requestLogin.AddParams(db, user, pass, XmlRpcParameter.EmptyStruct());

            XmlRpcResponse responseLogin = client.Execute(requestLogin);

            if (responseLogin.IsFault())
            {
                Console.WriteLine(responseLogin.GetFaultString());
            }
            else
            {
                Console.WriteLine(responseLogin.GetString());
            }
            client.Path = "object";
            XmlRpcRequest requestSearch = new XmlRpcRequest("execute_kw");
            requestSearch.AddParams(db, responseLogin.GetInt(), pass, "sale.order", "search",
                XmlRpcParameter.AsArray(
                    XmlRpcParameter.AsArray(
                        XmlRpcParameter.AsArray("name", "=", pedido)//campos para filtrar, puede ser orderhdr_id
                    )
                )
            );
            XmlRpcResponse responseSearch = client.Execute(requestSearch);

            if (responseSearch.IsFault())
            {
                Console.WriteLine(responseSearch.GetFaultString());
            }
            else
            {
                Console.WriteLine(responseSearch.GetString());
            }

            // READ
            //prueba para leer la venta, se puede eliminar
            XmlRpcRequest requestRead = new XmlRpcRequest("execute_kw");
            requestRead.AddParams(db, responseLogin.GetInt(), pass, "sale.order", "read",
                XmlRpcParameter.AsArray(
                    responseSearch.GetArray()
                )
            );

            requestRead.AddParamStruct(XmlRpcParameter.AsMember("fields",
                    XmlRpcParameter.AsArray("name")//campos de la vista que se buscan leer
                )
            );
            

            XmlRpcResponse responseRead = client.Execute(requestRead);
            Console.WriteLine("READ: ");
            if (responseRead.IsFault())
            {
                Console.WriteLine(responseRead.GetFaultString());
            }
            else
            {
                Console.WriteLine(responseRead.GetString());
            }
            //termina pruba para leer la venta

            //inicia comentario
            XmlRpcRequest comment = new XmlRpcRequest("execute_kw");
            comment.AddParams(db, responseLogin.GetInt(), pass, "sale.order", "message_post",
                XmlRpcParameter.AsArray(
                    responseSearch.GetArray()[0], "prueba"
                //responseSearch tendra un array de id que corrsepondan con el valor, solo necesitamos el primero si sabemos que solo hay un registro que pueda tener ese valor
                //el segundo parametro corresponde al texto plano que se colocara como comentario
                ));
            XmlRpcResponse responseComment = client.Execute(comment);
            Console.WriteLine("COMMENT: ");
            if (responseRead.IsFault())
            {
                Console.WriteLine(responseComment.GetFaultString());
            }
            else
            {
                Console.WriteLine(responseComment.GetString());
            }
        }

        public static List<UserResponse> PruebaBuscarPorCorreo(string[] idpedido)
        {
            List<UserResponse> resultados = new List<UserResponse>();

            XmlRpcClient client = new XmlRpcClient();
            client.Url = Url;
            client.Path = "common";

            // LOGIN

            XmlRpcRequest requestLogin = new XmlRpcRequest("authenticate");
            requestLogin.AddParams(db, user, pass, XmlRpcParameter.EmptyStruct());

            XmlRpcResponse responseLogin = client.Execute(requestLogin);

          

            // SEARCH

            client.Path = "object";

            //Recorre todos los correos electronicos
            foreach (var correo in idpedido)
            {
                XmlRpcRequest requestSearch = new XmlRpcRequest("execute_kw");
                requestSearch.AddParams(db, responseLogin.GetInt(), pass, "sale.order", "search",
                    XmlRpcParameter.AsArray(
                        XmlRpcParameter.AsArray(
                            //filtra donde el email sea igual (=) al dado
                            XmlRpcParameter.AsArray("id", "=", correo)
                        )
                    )
                );




                //ejecuta
                XmlRpcResponse responseSearch = client.Execute(requestSearch);



                // la respuesta de la ejecucion anterior se manda a leer

                XmlRpcRequest requestRead = new XmlRpcRequest("execute_kw");
                requestRead.AddParams(db, responseLogin.GetInt(), pass, "sale.order", "read",
                    XmlRpcParameter.AsArray(
                        responseSearch.GetArray()
                    )
                );

                //se le colocan los campos que estamos buscando
                requestRead.AddParamStruct(XmlRpcParameter.AsMember("fields",
                        XmlRpcParameter.AsArray("email", "customer_id", "customer_address_seq", "id")
                    )
                );
                //se ejecuta la lectura
                XmlRpcResponse responseRead = client.Execute(requestRead);





                //se almacena el resultado para no solicitarlo por cada iteracion
                var array = responseRead.GetArray();

                foreach (object item in responseRead.GetArray())
                {
                    //el resultado es compatible con el diccionario de c#
                    //cada elemento del diccionario es tratado como objeto y cada llave es el campo solicitado en la etapa anterior
                    Dictionary<string, object> datos = (Dictionary<string, object>)item;

                    //se almacenan los resultados, esto quiere decir que si mas de un cliente tiene el mismo correo, agregara mas de un resultado
                    //en caso de no encontrar nada, no se almacenara
                    resultados.Add(new UserResponse()
                    {
                        Id_Interno = int.Parse(datos["id"].ToString()),
                        Customer_Address_Seq = int.Parse(datos["customer_address_seq"].ToString()),//0 si no fue enviado al servicio web para think
                        Customer_Id = int.Parse(datos["customer_id"].ToString()),//0 si no fue enviado al servicio web para think
                        EMail = datos["email"].ToString(),
                    });
                }

            }
                //resultados.Add(new Resultado() {
                //    IdExterno = responseRead.
                //});
            

            return resultados;


           
        }

        public bool UpdateOdooNip(int Order_id,int Nip_Nuevo,int Nip_actual)
        {
            List<UserResponse> resultados = new List<UserResponse>();

            XmlRpcClient client = new XmlRpcClient();
            client.Url = Url;
            client.Path = "common";

            // LOGIN

            XmlRpcRequest requestLogin = new XmlRpcRequest("authenticate");
            requestLogin.AddParams(db, user, pass, XmlRpcParameter.EmptyStruct());

            XmlRpcResponse responseLogin = client.Execute(requestLogin);



            // SEARCH

            client.Path = "object";

            //Recorre todos los correos electronicos

            XmlRpcRequest requestSearch = new XmlRpcRequest("execute_kw");
            requestSearch.AddParams(db, responseLogin.GetInt(), pass, "sale.order", "search",
                XmlRpcParameter.AsArray(
                    XmlRpcParameter.AsArray(
                        //filtra donde el email sea igual (=) al dado
                        XmlRpcParameter.AsArray("id", "=", Order_id)
                    )
                )
            );

            //ejecuta
            XmlRpcResponse responseSearch = client.Execute(requestSearch);

            XmlRpcRequest Update = new XmlRpcRequest("execute_kw");
            Update.AddParams(db, responseLogin.GetInt(), pass, "sale.order", "write",
                XmlRpcParameter.AsArray(responseSearch.GetArray()[0],
                    XmlRpcParameter.AsStruct(
                          XmlRpcParameter.AsMember("x_studio_field_DGArF", Nip_Nuevo)
                    )
                )
            );

            XmlRpcResponse responseUpdate = client.Execute(Update);


            //var res = responseUpdate.GetBoolean();


            XmlRpcRequest notice = new XmlRpcRequest("execute_kw");
            notice.AddParams(db, responseLogin.GetInt(), pass, "sale.order", "message_post",
                XmlRpcParameter.AsArray(responseSearch.GetArray()[0], "Nip original:" + Nip_actual + " Nip nuevo:" + Nip_Nuevo
                    )

            );



            XmlRpcResponse responsenotice = client.Execute(notice);




            return true;

        }




        public static List<SaleResponse> GetSales(int IdClient)
        {
            List<SaleResponse> resultados = new List<SaleResponse>();

            XmlRpcClient client = new XmlRpcClient();
            client.Url = Url;
            client.Path = "common";

            // LOGIN

            XmlRpcRequest requestLogin = new XmlRpcRequest("authenticate");
            requestLogin.AddParams(db, user, pass, XmlRpcParameter.EmptyStruct());

            XmlRpcResponse responseLogin = client.Execute(requestLogin);

           

            // SEARCH

            client.Path = "object";

            //Recorre todos los correos electronicos
           
                XmlRpcRequest requestSearch = new XmlRpcRequest("execute_kw");
                requestSearch.AddParams(db, responseLogin.GetInt(), pass, "sale.order", "search",
                    XmlRpcParameter.AsArray(
                        XmlRpcParameter.AsArray(
                            //filtra donde el email sea igual (=) al dado
                            XmlRpcParameter.AsArray("partner_id", "=", IdClient)
                        )
                    )
                );

                //ejecuta
                XmlRpcResponse responseSearch = client.Execute(requestSearch);

              

                // la respuesta de la ejecucion anterior se manda a leer

                XmlRpcRequest requestRead = new XmlRpcRequest("execute_kw");
                requestRead.AddParams(db, responseLogin.GetInt(), pass, "sale.order", "read",
                    XmlRpcParameter.AsArray(
                        responseSearch.GetArray()
                    )
                );

                //se le colocan los campos que estamos buscando
                requestRead.AddParamStruct(XmlRpcParameter.AsMember("fields",
                        XmlRpcParameter.AsArray("delivery_method_id", "partnet_invoice_id", "name", "id")
                    )
                );
                //se ejecuta la lectura
                XmlRpcResponse responseRead = client.Execute(requestRead);



               

                //se almacena el resultado para no solicitarlo por cada iteracion
                var array = responseRead.GetArray();

                foreach (object item in responseRead.GetArray())
                {
                    //el resultado es compatible con el diccionario de c#
                    //cada elemento del diccionario es tratado como objeto y cada llave es el campo solicitado en la etapa anterior
                    Dictionary<string, object> datos = (Dictionary<string, object>)item;

                    //se almacenan los resultados, esto quiere decir que si mas de un cliente tiene el mismo correo, agregara mas de un resultado
                    //en caso de no encontrar nada, no se almacenara
                    resultados.Add(new SaleResponse()
                    {
                        delivery_method_id = int.Parse(datos["id"].ToString()),
                        partnet_invoice_id = int.Parse(datos["customer_address_seq"].ToString()),//0 si no fue enviado al servicio web para think
                        name = datos["customer_id"].ToString(),//0 si no fue enviado al servicio web para think
                        id = int.Parse( datos["email"].ToString())
                    });
                }

                //resultados.Add(new Resultado() {
                //    IdExterno = responseRead.
                //});
            

            return resultados;



        }


        public static List<UserResponse> GetLines(string IdSale)
        {
            List<UserResponse> resultados = new List<UserResponse>();

            XmlRpcClient client = new XmlRpcClient();
            client.Url = Url;
            client.Path = "common";

            // LOGIN

            XmlRpcRequest requestLogin = new XmlRpcRequest("authenticate");
            requestLogin.AddParams(db, user, pass, XmlRpcParameter.EmptyStruct());

            XmlRpcResponse responseLogin = client.Execute(requestLogin);

            //Console.WriteLine();
         

            // SEARCH

            client.Path = "object";

            //Recorre todos los correos electronicos
           
                XmlRpcRequest requestSearch = new XmlRpcRequest("execute_kw");
                requestSearch.AddParams(db, responseLogin.GetInt(), pass, "res.partner", "search",
                    XmlRpcParameter.AsArray(
                        XmlRpcParameter.AsArray(
                            //filtra donde el email sea igual (=) al dado
                            XmlRpcParameter.AsArray("email", "=", IdSale)
                        )
                    )
                );

                //ejecuta
                XmlRpcResponse responseSearch = client.Execute(requestSearch);

            
                // la respuesta de la ejecucion anterior se manda a leer

                XmlRpcRequest requestRead = new XmlRpcRequest("execute_kw");
                requestRead.AddParams(db, responseLogin.GetInt(), pass, "res.partner", "read",
                    XmlRpcParameter.AsArray(
                        responseSearch.GetArray()
                    )
                );

                //se le colocan los campos que estamos buscando
                requestRead.AddParamStruct(XmlRpcParameter.AsMember("fields",
                        XmlRpcParameter.AsArray("email", "customer_id", "customer_address_seq", "id")
                    )
                );
                //se ejecuta la lectura
                XmlRpcResponse responseRead = client.Execute(requestRead);


                //se almacena el resultado para no solicitarlo por cada iteracion
                var array = responseRead.GetArray();

                foreach (object item in responseRead.GetArray())
                {
                    //el resultado es compatible con el diccionario de c#
                    //cada elemento del diccionario es tratado como objeto y cada llave es el campo solicitado en la etapa anterior
                    Dictionary<string, object> datos = (Dictionary<string, object>)item;

                    //se almacenan los resultados, esto quiere decir que si mas de un cliente tiene el mismo correo, agregara mas de un resultado
                    //en caso de no encontrar nada, no se almacenara
                    resultados.Add(new UserResponse()
                    {
                        Id_Interno = int.Parse(datos["id"].ToString()),
                        Customer_Address_Seq = int.Parse(datos["customer_address_seq"].ToString()),//0 si no fue enviado al servicio web para think
                        Customer_Id = int.Parse(datos["customer_id"].ToString()),//0 si no fue enviado al servicio web para think
                        EMail = datos["email"].ToString(),
                    });
                }

                //resultados.Add(new Resultado() {
                //    IdExterno = responseRead.
                //});
            

            return resultados;



        }


        public static void TestReadRecords() {
            XmlRpcClient client = new XmlRpcClient();
            client.Url = Url;
            client.Path = "common";           

            // LOGIN

            XmlRpcRequest requestLogin = new XmlRpcRequest("authenticate");
            requestLogin.AddParams(db, user, pass, XmlRpcParameter.EmptyStruct());

            XmlRpcResponse responseLogin = client.Execute(requestLogin);

      

            Console.WriteLine();
            Console.WriteLine();
            Console.WriteLine("LOGIN: ");
            if (responseLogin.IsFault()) {
                Console.WriteLine(responseLogin.GetFaultString());
            } else {
                Console.WriteLine(responseLogin.GetString());
            }

            // SEARCH

            client.Path = "object";

            XmlRpcRequest requestSearch = new XmlRpcRequest("execute_kw");
            requestSearch.AddParams(db, responseLogin.GetInt(), pass, "res.partner", "search", 
                XmlRpcParameter.AsArray(
                    XmlRpcParameter.AsArray(
                        XmlRpcParameter.AsArray("is_company", "=", true), XmlRpcParameter.AsArray("customer", "=", true)
                    )
                )
            );

            requestSearch.AddParamStruct(
                XmlRpcParameter.AsMember("limit", 200)
            );

            XmlRpcResponse responseSearch = client.Execute(requestSearch);

          

            Console.WriteLine();
            Console.WriteLine();
            Console.WriteLine("SEARCH: ");
            if (responseSearch.IsFault()) {
                Console.WriteLine(responseSearch.GetFaultString());
            } else {
                Console.WriteLine(responseSearch.GetString());
            }

            // READ

            XmlRpcRequest requestRead = new XmlRpcRequest("execute_kw");
            requestRead.AddParams(db, responseLogin.GetInt(), pass, "res.partner", "read",                                           
                XmlRpcParameter.AsArray(
                    responseSearch.GetArray()
                )
            );

            requestRead.AddParamStruct(XmlRpcParameter.AsMember("fields", 
                    XmlRpcParameter.AsArray("name")
                )
            );

            XmlRpcResponse responseRead = client.Execute(requestRead);

           

            Console.WriteLine();
            Console.WriteLine();
            Console.WriteLine("READ: ");
            if (responseRead.IsFault()) {
                Console.WriteLine(responseRead.GetFaultString());
            } else {
                Console.WriteLine(responseRead.GetString());
            }
        }

        public static void TestResponseXml() {
            XmlDocument testDoc = new XmlDocument();
            // testDoc.AppendChild(testDoc.CreateElement("methodResponse"));
            // testDoc.LoadXml("<methodResponse><fault><value><struct><member><name>faultCode</name><value><int>1</int></value></member><member><name>faultString</name><value><string>Error</string></value></member></struct></value></fault></methodResponse>");
            testDoc.LoadXml("<methodResponse><params><param><value><array><data><value><int>7</int></value><value><int>11</int></value><value><int>8</int></value><value><int>44</int></value><value><int>10</int></value><value><int>12</int></value></data></array></value></param></params></methodResponse>");

            testDoc.Save(Console.Out);
            XmlRpcResponse response = ResponseFactory.BuildResponse(testDoc);

            if (response.IsFault()) {
                Console.WriteLine(response.GetFaultString());
            } else {
                Console.WriteLine();
                Console.WriteLine();
                Console.WriteLine(response.GetString());
            }
        }     

        public static void TestSearchReadRecords() {
            XmlRpcClient client = new XmlRpcClient();
            client.Url = Url;
            client.Path = "common";           

            // LOGIN

            XmlRpcRequest requestLogin = new XmlRpcRequest("authenticate");
            requestLogin.AddParams(db, user, pass, XmlRpcParameter.EmptyStruct());

            XmlRpcResponse responseLogin = client.Execute(requestLogin);

          


            Console.WriteLine();
            Console.WriteLine();
            Console.WriteLine("LOGIN: ");
            if (responseLogin.IsFault()) {
                Console.WriteLine(responseLogin.GetFaultString());
            } else {
                Console.WriteLine(responseLogin.GetString());
            }

            // SEARCH

            client.Path = "object";

            XmlRpcRequest requestSearch = new XmlRpcRequest("execute_kw");
            requestSearch.AddParams(db, responseLogin.GetInt(), pass, "res.partner", "search_read", 
                XmlRpcParameter.AsArray(
                    XmlRpcParameter.AsArray(
                        // XmlRpcParameter.AsArray("is_company", "=", true), XmlRpcRequest.AsArray("customer", "=", true)
                        XmlRpcParameter.AsArray("name", "ilike", "t")
                    )
                ),
                XmlRpcParameter.AsStruct(
                    XmlRpcParameter.AsMember("fields", XmlRpcParameter.AsArray("name", "email"))
                    // ,XmlRpcParameter.AsMember("limit", 2)
                )
            );                      

            XmlRpcResponse responseSearch = client.Execute(requestSearch);

            Console.WriteLine();
            Console.WriteLine();
            Console.WriteLine("search");
            Console.WriteLine("REQUEST: ");
            client.WriteRequest(Console.Out);

            Console.WriteLine();
            Console.WriteLine();
            Console.WriteLine("RESPONSE: ");
            

            Console.WriteLine();
            Console.WriteLine();
            Console.WriteLine("SEARCH: ");
            if (responseSearch.IsFault()) {
                Console.WriteLine(responseSearch.GetFaultString());
            } else {
                Console.WriteLine(responseSearch.GetString());
            }
           
        }

        public static void TestCreateRecord() {
            XmlRpcClient client = new XmlRpcClient();
            client.Url = Url;
            client.Path = "common";           

            // LOGIN

            XmlRpcRequest requestLogin = new XmlRpcRequest("authenticate");
            requestLogin.AddParams(db, user, pass, XmlRpcParameter.EmptyStruct());

            XmlRpcResponse responseLogin = client.Execute(requestLogin);

            Console.WriteLine("authenticate");
            Console.WriteLine("REQUEST: ");
            client.WriteRequest(Console.Out);

            Console.WriteLine();
            Console.WriteLine();
            Console.WriteLine("RESPONSE: ");
            client.WriteResponse(Console.Out);

            Console.WriteLine();
            Console.WriteLine();
            Console.WriteLine("LOGIN: ");
            if (responseLogin.IsFault()) {
                Console.WriteLine(responseLogin.GetFaultString());
            } else {
                Console.WriteLine(responseLogin.GetString());
            }

            // CREATE

            client.Path = "object"; 

            XmlRpcRequest requestCreate = new XmlRpcRequest("execute_kw");
            requestCreate.AddParams(db, responseLogin.GetInt(), pass, "res.partner", "create",                                           
                XmlRpcParameter.AsArray(
                    XmlRpcParameter.AsStruct(
                        XmlRpcParameter.AsMember("name", "Albert Einstein")
                        , XmlRpcParameter.AsMember("image", Convert.ToBase64String(File.ReadAllBytes("img/einstein.jpg")))
                        , XmlRpcParameter.AsMember("email", "albert.einstein@email.com")
                    )
                )
            );

            XmlRpcResponse responseCreate = client.Execute(requestCreate);

            Console.WriteLine();
            Console.WriteLine();
            Console.WriteLine("create");
            Console.WriteLine("REQUEST: ");
            client.WriteRequest(Console.Out);

            Console.WriteLine();
            Console.WriteLine();
            Console.WriteLine("RESPONSE: ");
            client.WriteResponse(Console.Out);

            Console.WriteLine();
            Console.WriteLine();
            Console.WriteLine("READ: ");
            if (responseCreate.IsFault()) {
                Console.WriteLine(responseCreate.GetFaultString());
            } else {
                Console.WriteLine(responseCreate.GetString());
            }
        }

        public static void Main(string[] args) {
            // TestRequestXml();
            // TestResponseXml();
            //TestReadVersion();
            //TestReadRecords();
            //TestCreateRecord();
            // TestSearchReadRecords();
            //PruebaComentarPedido("SO198");

            string[] correos = { 
                                    "ing.carlosjl@gmail.com"
                              };

            
            List<UserResponse> encontrados = PruebaBuscarPorCorreo(correos);
            foreach (UserResponse item in encontrados)
            {
                Console.WriteLine(item.ToString());
                List<SaleResponse> Sales = GetSales(item.Id_Interno);
                
            }
            Console.ReadKey();
        }
        //public class Resultado
        //{
        //    public int Id_Interno { get; set; }
        //    public int Customer_Address_Seq { get; set; }
        //    public int Customer_Id { get; set; }
        //    public string EMail { get; set; }

        //    public override string ToString()
        //    {
        //        return $"Id_interno={Id_Interno}, Customer_Address_Seq={Customer_Address_Seq}, Customer_Id={Customer_Id}, EMail={EMail}";
        //    }

        //}

        //public class SalesResponse
        //{

        //    public int delivery_method_id { get; set; }
        //    public int partnet_invoice_id { get; set; }
        //    public string name { get; set; }
        //    public int id { get; set; }

        //    public override string ToString()
        //    {
        //        return $"delivery_method_id={delivery_method_id}, partnet_invoice_id={partnet_invoice_id}, name={name}, id={id}";
        //    }

        //}
    }
}
