﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Data.SqlClient;


namespace WebApplication2
{
    public partial class About : Page
    {
        public String SQL = "";
        public SqlDataReader rs;
        public SqlDataReader rs2;
        public SqlConnection myConnection;
        public SqlDataReader myReader;
        public SqlConnection myConnection2;
        public SqlDataReader myReader2;
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void Page_PreRender(object sender, EventArgs e)
        {
            if (myConnection != null)
            {
                if ((int)myConnection.State == 1)
                {
                    myConnection.Close();
                }
            }

            if (myConnection2 != null)
            {
                if ((int)myConnection2.State == 1)
                {
                    myConnection2.Close();
                }
            }
        }

        public SqlDataReader conex(string qry)
        {
            //recuperamos la conexion del config
            Configuration rootWebConfig = ConfigurationManager.OpenExeConfiguration("");
            ConnectionStringSettings connString;
            //verifica que exista al menos una cadena de conexión
            if (0 < rootWebConfig.ConnectionStrings.ConnectionStrings.Count)
            {
                //lee la cadena de conexión
                connString = rootWebConfig.ConnectionStrings.ConnectionStrings["ConexionAcceso"];
                //verifica que exista la cadena de conexión especificada
                if (null != connString)
                {
                    //crea la cadena de conexión

                    //abre la conexión
                    try
                    {
                        if (myConnection == null)
                        {
                            myConnection = new SqlConnection(connString.ConnectionString);
                            myConnection.Open();
                        }
                        //ejecuta el comando
                        try
                        {
                            SqlCommand myCommand = new SqlCommand(qry, myConnection);
                            myCommand.CommandTimeout = 5000000;
                            if (myReader != null)
                            {
                                myReader.Dispose();
                            }
                            myReader = myCommand.ExecuteReader();
                        }
                        catch (Exception e)
                        {
                            throw new System.ArgumentException("conn error", e);
                        }
                    }
                    catch (Exception e)
                    {
                        throw new System.ArgumentException("db conn error 1", e);
                    }
                    //finally { myConnection.Close(); }
                    return myReader;
                }
                else
                    throw new System.ArgumentException("ConnString is null", "original");
            }
            else
                throw new System.ArgumentException("ConnStrings is empty", "original");

        }

        public SqlDataReader conex3(string qry)
        {
            //recuperamos la conexion del config
            Configuration rootWebConfig = ConfigurationManager.OpenExeConfiguration("");
            ConnectionStringSettings connString;
            //verifica que exista al menos una cadena de conexión
            if (0 < rootWebConfig.ConnectionStrings.ConnectionStrings.Count)
            {
                //lee la cadena de conexión
                connString = rootWebConfig.ConnectionStrings.ConnectionStrings["ConexionAcceso"];
                //verifica que exista la cadena de conexión especificada
                if (null != connString)
                {
                    //crea la cadena de conexión

                    //abre la conexión
                    try
                    {
                        if (myConnection2 == null)
                        {
                            myConnection2 = new SqlConnection(connString.ConnectionString);
                            myConnection2.Open();
                        }
                        //ejecuta el comando
                        try
                        {
                            SqlCommand myCommand = new SqlCommand(qry, myConnection2);
                            myCommand.CommandTimeout = 5000000;
                            if (myReader2 != null)
                            {
                                myReader2.Dispose();
                            }
                            myReader2 = myCommand.ExecuteReader();
                        }
                        catch (Exception e)
                        {
                            throw new System.ArgumentException("conn error", e);
                        }
                    }
                    catch (Exception e)
                    {
                        throw new System.ArgumentException("db conn error 1", e);
                    }
                    //finally { myConnection.Close(); }
                    return myReader2;
                }
                else
                    throw new System.ArgumentException("ConnString is null", "original");
            }
            else
                throw new System.ArgumentException("ConnStrings is empty", "original");

        }

        //escritura
        public SqlDataReader conex2(string qry)
        {
            SqlDataReader myReader = null;
            Configuration rootConfig = ConfigurationManager.OpenExeConfiguration("");
            ConnectionStringSettings connString;
            if (0 < rootConfig.ConnectionStrings.ConnectionStrings.Count)
            {
                connString = rootConfig.ConnectionStrings.ConnectionStrings["ConexionAcceso"];
                if (null != connString)
                {
                    SqlConnection myConnection2 = new SqlConnection(connString.ConnectionString);
                    try
                    {
                        myConnection2.Open();
                        try
                        {
                            SqlCommand myCommand = new SqlCommand(qry, myConnection2);
                            //myCommand.CommandTimeout = 5000;
                            myCommand.ExecuteNonQuery();
                        }
                        catch (Exception e)
                        {
                            throw new System.ArgumentException("conn error", e);
                        }
                    }
                    catch (Exception e)
                    {
                        throw new System.ArgumentException("db conn error 1", e);
                    }
                    //finally { myConnection2.Close(); }
                    return myReader;
                }
                else
                    throw new System.ArgumentException("ConnString is null", "original");
            }
            else
                throw new System.ArgumentException("ConnStrings is empty", "original");
        }

        public void libera()
        {
            if (myReader != null)
            {
                myReader.Dispose();
            }
            if (myReader2 != null)
            {
                myReader2.Dispose();
            }
            if (myConnection != null)
            {
                myConnection.Close();
            }
            if (myConnection2 != null)
            {
                myConnection2.Close();
            }

        }

        protected void Button1_Click1(object sender, EventArgs e)
        {
            String a = "", b = "", c = "", d = "", nombre="", orderhdr_id="", order_item_seq="";
            int llamadas_r = 0;
            int llamadas_f = 0;
            a = TextBox1.Text; //nip
            b = TextBox2.Text; // tema
            c = DropDownList1.SelectedValue.ToString(); //consultor
            d = TextBox3.Text; //cantidad
            
            string guid = Guid.NewGuid().ToString();

            if (a != "" && c != "xxx" && b != "" && d != "")
            {

                SQL = "select * from fac0mxdb..CLIENTES where NIP=" + a;
                rs = conex(SQL);
                if (rs.HasRows)
                {
                    rs.Read();
                    llamadas_r = Convert.ToInt16(rs["TOTLLAREA"].ToString());
                    nombre = rs["NOMSUS"].ToString();
                    order_item_seq= rs["order_item_seq"].ToString();
                    orderhdr_id = rs["orderhdr_id"].ToString();
                    llamadas_f = llamadas_r + Convert.ToInt16(d);
                    SQL = "UPDATE  fac0mxdb..CLIENTES set TOTLLAREA=" + llamadas_f +"  where NIP=" + a;
                    conex2(SQL);
                    SQL = "INSERT INTO fac0mxdb..Registro_Llamada2 (nip_cliente,id_usuario,nombre_llama, num_consultas, califica_emp, tema, log_registro, guid,periodo,orderhdr_id,order_item_seq, depuradoBi) VAlUES " +
                            "(" + a + ",'" + c + "','" + nombre + "'," + d + ",'',' " + b + "',GETDATE(),'" + guid + "','', " + orderhdr_id + "," + order_item_seq + ",1)";
                    conex2(SQL);

                }                
                else
                {
                    Response.Write("<script LANGUAGE='JavaScript' >alert('El NIP no existe')</script>");
                }


                libera();

                Response.Write("<script LANGUAGE='JavaScript' >alert('Se han descontado el numero de llamadas indicadas')</script>");
                TextBox1.Text = "";
                TextBox2.Text = "";
                TextBox3.Text = "";

            }
            else
            {
                Response.Write("<script LANGUAGE='JavaScript' >alert('favor de llenar el formulario')</script>");
            }
        }
    }
}