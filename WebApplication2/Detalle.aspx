﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Detalle.aspx.cs" Inherits="WebApplication2.WebForm1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <div class="">
        <h1>CONSULTORIA IDC-DETALLE LLAMADAS</h1>               
    </div>

     <div class="row">        
        <div class="col-md-2"><b>Buscar NIP</b> </div>
        <div class="col-md-3"><asp:TextBox ID="TextBox1" runat="server" CssClass="form-control" Width="550px"></asp:TextBox></div>        
        <div class="col-md-3"><asp:Button ID="Button1" runat="server" Text="BUSCAR" CssClass="btn btn-primary" OnClick="Button1_Click" /> 
            <br />
            <br />
         </div>
        
         <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" CssClass="table table-responsive  table-striped table-hover" CellPadding="4" ForeColor="#333333" GridLines="None">
             <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
             <Columns>
                 <asp:BoundField DataField="nip_cliente" HeaderText="NIP" />                 
                 <asp:BoundField DataField="id_Usuario" HeaderText="CUSTOMER_ID" />
                 <asp:BoundField DataField="nombre_llama" HeaderText="NOMBRE" />
                 <asp:BoundField DataField="num_consultas" HeaderText="NUM CONSULTAS" />
                 <asp:BoundField DataField="tema" HeaderText="TEMA" />
                 <asp:BoundField DataField="log_registro" HeaderText="FECHA" />
             </Columns>
             <EditRowStyle BackColor="#999999" />
             <FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
             <HeaderStyle CssClass="thead-dark" BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
             <PagerStyle BackColor="#284775" ForeColor="White" HorizontalAlign="Center" />
             <RowStyle BackColor="#F7F6F3" ForeColor="#333333" />
             <SelectedRowStyle BackColor="#E2DED6" Font-Bold="True" ForeColor="#333333" />
             <SortedAscendingCellStyle BackColor="#E9E7E2" />
             <SortedAscendingHeaderStyle BackColor="#506C8C" />
             <SortedDescendingCellStyle BackColor="#FFFDF8" />
             <SortedDescendingHeaderStyle BackColor="#6F8DAE" />
         </asp:GridView>
    </div>

</asp:Content>
