﻿<%@ Page Title="Contact" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Contact.aspx.cs" Inherits="WebApplication2.Contact" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <div class="">
        <h1>CONSULTORIA IDC-CAMBIAR NIP</h1>               
    </div>
    <br />
    <br />
    <div class="row">  
        <asp:Label ID="Label1" runat="server" Text="SE DEBEN LLENAR TODOS LOS CAMPOS" CssClass="alert alert-info"></asp:Label><br /><br />

        <asp:Table ID="Table1" runat="server" Visible="true" GridLines="Horizontal" Caption="DATOS" CssClass="table table-striped">
        <asp:TableRow>
            <asp:TableHeaderCell>NIP Actual:</asp:TableHeaderCell>
            <asp:TableCell>   <asp:TextBox ID="TextBox1" runat="server" Enabled="false"  CssClass="form-control" ></asp:TextBox> </asp:TableCell>
            <asp:TableHeaderCell>NIP NUEVO:</asp:TableHeaderCell>
            <asp:TableCell>
                <asp:TextBox ID="TextBox2" runat="server"  CssClass="form-control" ></asp:TextBox></asp:TableCell>
        </asp:TableRow>
        <asp:TableRow>
            <asp:TableHeaderCell>EJ ATC:</asp:TableHeaderCell>
            <asp:TableCell> <asp:DropDownList ID="DropDownList1" runat="server"  CssClass="form-control" >
                <asp:ListItem Value="">Seleccione un consultor</asp:ListItem>
                <asp:ListItem Value="jnombre">Juana Nombre</asp:ListItem>
                <asp:ListItem Value="gvidal">Gustavo Vidal</asp:ListItem>
                <asp:ListItem Value="jnieves">Jocabeth Nieves</asp:ListItem>
                
                            </asp:DropDownList></asp:TableCell>
            <asp:TableHeaderCell>MOTIVO:</asp:TableHeaderCell>
            <asp:TableCell>
                <asp:TextBox ID="TextBox3" runat="server"  CssClass="form-control" ></asp:TextBox> </asp:TableCell>
        </asp:TableRow>
        <asp:TableRow>
            <asp:TableHeaderCell>Key:</asp:TableHeaderCell>
            <asp:TableCell>   <asp:TextBox ID="TextBox9" runat="server" Enabled="false"  CssClass="form-control" ></asp:TextBox> </asp:TableCell>
            <asp:TableHeaderCell></asp:TableHeaderCell>
            <asp:TableCell><asp:Button ID="Button2" runat="server" Text="GENERAR" CssClass="btn btn-info" OnClick="Button2_Click1" Enabled="false"></asp:Button>
                </asp:TableCell>
        </asp:TableRow>
    </asp:Table>
    <br />

         <center>
        <asp:Button ID="Button1" runat="server" Text="CAMBIAR" CssClass="btn btn-warning" OnClick="Button1_Click1" Enabled="false"></asp:Button>
        

    </center>
    </div>
</asp:Content>
