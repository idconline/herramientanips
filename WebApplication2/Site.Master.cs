﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WebApplication2
{
    public partial class SiteMaster : MasterPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            HttpCookie cookie = Request.Cookies.Get("UserID");
            if (cookie == null)
            {
                Label11.Text = "SIN COOKIE";
                Response.Redirect("http://serviciowebidc.idconline.mx/CONSULTORIA/Login");

            }
            else
            {
                String data = Request.Cookies["UserID"].Value;
                string[] words = data.Split('|');
                Label11.Text = words[1];
                Image1.ImageUrl = words[2];
            }
        }
    }
}