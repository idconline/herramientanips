﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Update.aspx.cs" Inherits="WebApplication2.WebForm2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <div class="">
        <h1>CONSULTORIA IDC-DESCONTAR LLAMADAS MANUALES</h1>               
    </div>
    <br />
    <br />
    <div class="row">        
        <div class="col-md-2"><b>Buscar NIP</b> </div>
        <div class="col-md-3"><asp:TextBox ID="TextBox1" runat="server" CssClass="form-control" Width="550px"></asp:TextBox></div>        
        <div class="col-md-3"><asp:Button ID="Button1" runat="server" Text="BUSCAR" CssClass="btn btn-primary" OnClick="Button1_Click" /> 
            <br />
            <br />
         </div>
    </div>
    <br />
    <br />
    <asp:Table ID="Table1" runat="server" Visible="true" GridLines="Horizontal" Caption="DATOS" CssClass="table table-striped">
        <asp:TableRow>
            <asp:TableHeaderCell>EMPRESA:</asp:TableHeaderCell>
            <asp:TableCell> <asp:TextBox ID="TextBox2" runat="server"  CssClass="form-control"></asp:TextBox> </asp:TableCell>
            <asp:TableHeaderCell>NIP:</asp:TableHeaderCell>
            <asp:TableCell><asp:Label ID="Label6" runat="server" Text="Label"></asp:Label> </asp:TableCell>
        </asp:TableRow>
        <asp:TableRow>
            <asp:TableHeaderCell>SUSCRIPTOR:</asp:TableHeaderCell>
            <asp:TableCell>                <asp:TextBox ID="TextBox3"  CssClass="form-control" runat="server"></asp:TextBox>  </asp:TableCell>
            <asp:TableHeaderCell>No CLIENTE:</asp:TableHeaderCell>
            <asp:TableCell><asp:Label ID="Label7" runat="server" Text="Label"></asp:Label> </asp:TableCell>
        </asp:TableRow>
        <asp:TableRow>
            <asp:TableHeaderCell>TELEFONO:</asp:TableHeaderCell>
            <asp:TableCell>                <asp:TextBox ID="TextBox4"  CssClass="form-control" runat="server"></asp:TextBox>  </asp:TableCell>
            <asp:TableHeaderCell>PERIODO</asp:TableHeaderCell>
            <asp:TableCell><asp:Label ID="Label8" runat="server" Text="Label"></asp:Label> </asp:TableCell>
        </asp:TableRow>
        <asp:TableRow>
            <asp:TableHeaderCell>CORREO</asp:TableHeaderCell>
            <asp:TableCell>                <asp:TextBox ID="TextBox5" runat="server" CssClass="form-control"></asp:TextBox> </asp:TableCell>
            <asp:TableHeaderCell>ESTATUS</asp:TableHeaderCell>
            <asp:TableCell><asp:Label ID="Label9" runat="server" Text="Label"></asp:Label> </asp:TableCell>
        </asp:TableRow>
        <asp:TableRow>
            <asp:TableHeaderCell>ADQUIRIDAS</asp:TableHeaderCell>
            <asp:TableCell><asp:Label ID="Label10" runat="server" Text="Label"></asp:Label> </asp:TableCell>
            <asp:TableHeaderCell>REALIZADAS</asp:TableHeaderCell>
            <asp:TableCell><asp:Label ID="Label11" runat="server" Text="Label"></asp:Label> </asp:TableCell>
        </asp:TableRow>
        <asp:TableRow>
            <asp:TableHeaderCell>DISPONIBLES</asp:TableHeaderCell>
            <asp:TableCell><asp:Label ID="Label12" runat="server" Text="Label"></asp:Label> </asp:TableCell>
            <asp:TableHeaderCell>DETALLE</asp:TableHeaderCell>
            <asp:TableCell>
                <asp:Button ID="Button2" runat="server" Text="Actualizar" CssClass="btn btn-success" OnClick="Button2_Click" /> </asp:TableCell>
        </asp:TableRow>
    </asp:Table>
</asp:Content>
