﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Data.SqlClient;
using System.Data;

namespace WebApplication2
{
    public partial class WebForm2 : System.Web.UI.Page
    {
        public String SQL = "";
        public SqlDataReader rs;
        public SqlDataReader rs2;
        public SqlConnection myConnection;
        public SqlDataReader myReader;
        public SqlConnection myConnection2;
        public SqlDataReader myReader2;
        public DataTable dt = new DataTable();
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        public SqlDataReader conex(string qry)
        {
            //recuperamos la conexion del config
            Configuration rootWebConfig = ConfigurationManager.OpenExeConfiguration("");
            ConnectionStringSettings connString;
            //verifica que exista al menos una cadena de conexión
            if (0 < rootWebConfig.ConnectionStrings.ConnectionStrings.Count)
            {
                //lee la cadena de conexión
                connString = rootWebConfig.ConnectionStrings.ConnectionStrings["ConexionAcceso"];
                //verifica que exista la cadena de conexión especificada
                if (null != connString)
                {
                    //crea la cadena de conexión

                    //abre la conexión
                    try
                    {
                        if (myConnection == null)
                        {
                            myConnection = new SqlConnection(connString.ConnectionString);
                            myConnection.Open();
                        }
                        //ejecuta el comando
                        try
                        {
                            SqlCommand myCommand = new SqlCommand(qry, myConnection);
                            myCommand.CommandTimeout = 5000000;
                            if (myReader != null)
                            {
                                myReader.Dispose();
                            }
                            myReader = myCommand.ExecuteReader();
                        }
                        catch (Exception e)
                        {
                            throw new System.ArgumentException("conn error", e);
                        }
                    }
                    catch (Exception e)
                    {
                        throw new System.ArgumentException("db conn error 1", e);
                    }
                    //finally { myConnection.Close(); }
                    return myReader;
                }
                else
                    throw new System.ArgumentException("ConnString is null", "original");
            }
            else
                throw new System.ArgumentException("ConnStrings is empty", "original");

        }

        public SqlDataReader conex3(string qry)
        {
            //recuperamos la conexion del config
            Configuration rootWebConfig = ConfigurationManager.OpenExeConfiguration("");
            ConnectionStringSettings connString;
            //verifica que exista al menos una cadena de conexión
            if (0 < rootWebConfig.ConnectionStrings.ConnectionStrings.Count)
            {
                //lee la cadena de conexión
                connString = rootWebConfig.ConnectionStrings.ConnectionStrings["ConexionAcceso"];
                //verifica que exista la cadena de conexión especificada
                if (null != connString)
                {
                    //crea la cadena de conexión

                    //abre la conexión
                    try
                    {
                        if (myConnection2 == null)
                        {
                            myConnection2 = new SqlConnection(connString.ConnectionString);
                            myConnection2.Open();
                        }
                        //ejecuta el comando
                        try
                        {
                            SqlCommand myCommand = new SqlCommand(qry, myConnection2);
                            myCommand.CommandTimeout = 5000000;
                            if (myReader2 != null)
                            {
                                myReader2.Dispose();
                            }
                            myReader2 = myCommand.ExecuteReader();
                        }
                        catch (Exception e)
                        {
                            throw new System.ArgumentException("conn error", e);
                        }
                    }
                    catch (Exception e)
                    {
                        throw new System.ArgumentException("db conn error 1", e);
                    }
                    //finally { myConnection.Close(); }
                    return myReader2;
                }
                else
                    throw new System.ArgumentException("ConnString is null", "original");
            }
            else
                throw new System.ArgumentException("ConnStrings is empty", "original");

        }

        //escritura
        public SqlDataReader conex2(string qry)
        {
            SqlDataReader myReader = null;
            Configuration rootConfig = ConfigurationManager.OpenExeConfiguration("");
            ConnectionStringSettings connString;
            if (0 < rootConfig.ConnectionStrings.ConnectionStrings.Count)
            {
                connString = rootConfig.ConnectionStrings.ConnectionStrings["ConexionAcceso"];
                if (null != connString)
                {
                    SqlConnection myConnection2 = new SqlConnection(connString.ConnectionString);
                    try
                    {
                        myConnection2.Open();
                        try
                        {
                            SqlCommand myCommand = new SqlCommand(qry, myConnection2);
                            //myCommand.CommandTimeout = 5000;
                            myCommand.ExecuteNonQuery();
                        }
                        catch (Exception e)
                        {
                            throw new System.ArgumentException("conn error", e);
                        }
                    }
                    catch (Exception e)
                    {
                        throw new System.ArgumentException("db conn error 1", e);
                    }
                    //finally { myConnection2.Close(); }
                    return myReader;
                }
                else
                    throw new System.ArgumentException("ConnString is null", "original");
            }
            else
                throw new System.ArgumentException("ConnStrings is empty", "original");
        }

        public void libera()
        {
            if (myReader != null)
            {
                myReader.Dispose();
            }
            if (myReader2 != null)
            {
                myReader2.Dispose();
            }
            if (myConnection != null)
            {
                myConnection.Close();
            }
            if (myConnection2 != null)
            {
                myConnection2.Close();
            }

        }
        protected void Page_PreRender(object sender, EventArgs e)
        {
            if (myConnection != null)
            {
                if ((int)myConnection.State == 1)
                {
                    myConnection.Close();
                }
            }

            if (myConnection2 != null)
            {
                if ((int)myConnection2.State == 1)
                {
                    myConnection2.Close();
                }
            }
        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            if (TextBox1.Text.Length > 0)
            {
                String a = "";
                a = TextBox1.Text;

                SQL = "select (TOTNUMLLA-TOTLLAREA) ,* from fac0mxdb..CLIENTES where NIP='" + a + "'";
                rs = conex(SQL);
                if (rs.HasRows)
                {
                    rs.Read();
                
                    //Label1.Text = "<b>El NIP esta activo y le quedan <b>" + x.ToString() + "</b> llamadas disponibles</b>";


                    Table1.Visible = true;
                    TextBox2.Text = rs[4].ToString();//empresa
                    Label6.Text = "<b>" + rs[1].ToString() + "</b>";//nip
                    TextBox3.Text = rs[3].ToString();//suscriptor
                    Label7.Text = rs[2].ToString();//no cliente
                    TextBox4.Text = rs[10].ToString();//telefono
                    Label8.Text = rs[16].ToString() + " <b>/</b> " + rs[17].ToString();//periodo
                    TextBox5.Text = rs[23].ToString();//correo
                    if (Convert.ToInt16(rs["STATUS"].ToString()) == 1)
                        Label9.Text = "ACTIVO";//estatus
                    else
                        Label9.Text = "DESACTIVADO";//estatus

                    Label10.Text = rs[18].ToString();//adquiridas
                    Label11.Text = rs[19].ToString();//realizadas
                    Label12.Text = rs[0].ToString();//disponibles
                    ;

                }
                
            }
        }

        protected void Button2_Click(object sender, EventArgs e)
        {
            String f, b, c, d;
            String a = "";
            a = TextBox1.Text;//nip
            f = TextBox2.Text;//empresa
            b = TextBox3.Text;//suscriptor
            c = TextBox4.Text;//telefono
            d = TextBox5.Text;//correo

            SQL = "UPDATE  fac0mxdb..CLIENTES set NOMEMP='" + f + "', NOMSUS='" + b + "', NUMTELE1='" + c + "', email='" + d + "'  where NIP=" + a;
            conex2(SQL);

        }

    }
}