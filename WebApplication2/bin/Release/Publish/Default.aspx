﻿<%@ Page Title="Home Page" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="WebApplication2._Default" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">

    <div class="">
        <h1>CONSULTORIA IDC</h1>         
    </div>

    <div class="row">        
        <div class="col-md-2"><b>Buscar NIP</b> </div>
        <div class="col-md-3"><asp:TextBox ID="TextBox1" runat="server" CssClass="form-control" Width="550px"></asp:TextBox></div>
        <div class="col-md-3"><asp:DropDownList ID="DropDownList1" runat="server" CssClass="form-control">            
            <asp:ListItem Value="1">NIP</asp:ListItem>
            <asp:ListItem Value="2">EMAIL</asp:ListItem>
            <asp:ListItem Value="3">No Cliente</asp:ListItem>
            </asp:DropDownList></div>
        <div class="col-md-3"><asp:Button ID="Button1" runat="server" Text="BUSCAR" CssClass="btn btn-primary" OnClick="Button1_Click" /> </div>
        
        
    </div>
    <br />
    <div id="DivAlert" class="" runat="server"  >
        <strong>-</strong><asp:Label ID="Label1" runat="server" Text=""></asp:Label>
    </div>
    <br />
    <br />
    <asp:Table ID="Table1" runat="server" Visible="false" GridLines="Horizontal" Caption="DATOS" CssClass="table table-striped">
        <asp:TableRow>
            <asp:TableHeaderCell>EMPRESA:</asp:TableHeaderCell>
            <asp:TableCell><asp:Label ID="Label2" runat="server" Text="Label"></asp:Label> </asp:TableCell>
            <asp:TableHeaderCell>NIP:</asp:TableHeaderCell>
            <asp:TableCell><asp:Label ID="Label6" runat="server" Text="Label"></asp:Label> </asp:TableCell>
        </asp:TableRow>
        <asp:TableRow>
            <asp:TableHeaderCell>SUSCRIPTOR:</asp:TableHeaderCell>
            <asp:TableCell><asp:Label ID="Label3" runat="server" Text="Label"></asp:Label> </asp:TableCell>
            <asp:TableHeaderCell>No CLIENTE:</asp:TableHeaderCell>
            <asp:TableCell><asp:Label ID="Label7" runat="server" Text="Label"></asp:Label> </asp:TableCell>
        </asp:TableRow>
        <asp:TableRow>
            <asp:TableHeaderCell>TELEFONO:</asp:TableHeaderCell>
            <asp:TableCell><asp:Label ID="Label4" runat="server" Text="Label"></asp:Label> </asp:TableCell>
            <asp:TableHeaderCell>PERIODO</asp:TableHeaderCell>
            <asp:TableCell><asp:Label ID="Label8" runat="server" Text="Label"></asp:Label> </asp:TableCell>
        </asp:TableRow>
        <asp:TableRow>
            <asp:TableHeaderCell>CORREO</asp:TableHeaderCell>
            <asp:TableCell><asp:Label ID="Label5" runat="server" Text="Label"></asp:Label> </asp:TableCell>
            <asp:TableHeaderCell>ESTATUS</asp:TableHeaderCell>
            <asp:TableCell><asp:Label ID="Label9" runat="server" Text="Label"></asp:Label> </asp:TableCell>
        </asp:TableRow>
        <asp:TableRow>
            <asp:TableHeaderCell>ADQUIRIDAS</asp:TableHeaderCell>
            <asp:TableCell><asp:Label ID="Label10" runat="server" Text="Label"></asp:Label> </asp:TableCell>
            <asp:TableHeaderCell>REALIZADAS</asp:TableHeaderCell>
            <asp:TableCell><asp:Label ID="Label11" runat="server" Text="Label"></asp:Label> </asp:TableCell>
        </asp:TableRow>
        <asp:TableRow>
            <asp:TableHeaderCell>DISPONIBLES</asp:TableHeaderCell>
            <asp:TableCell><asp:Label ID="Label12" runat="server" Text="Label"></asp:Label> </asp:TableCell>
            <asp:TableHeaderCell>DETALLE</asp:TableHeaderCell>
            <asp:TableCell> <asp:HyperLink ID="HyperLink1" runat="server" Text="llamadas" ></asp:HyperLink> </asp:TableCell>
        </asp:TableRow>
    </asp:Table>
    <br />
    <div class="row">
        <asp:GridView ID="GridView1" runat="server" CssClass="table table-responsive  table-striped table-hover" 
            CellPadding="4" ForeColor="#333333" GridLines="None" AutoGenerateColumns="False"
             OnSelectedIndexChanged="GridView1_SelectedIndexChanged">
            <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
            <Columns>
                <asp:BoundField DataField="NIP" HeaderText="NIP" />
                <asp:BoundField DataField="NUMSUS" HeaderText="CUSTOMER ID" />
                <asp:BoundField DataField="NOMSUS" HeaderText="NOMBRE" />
                <asp:BoundField DataField="NOMEMP" HeaderText="EMPRESA" />
                <asp:BoundField DataField="NUMTELE1" HeaderText="TELEFONO" />
                <asp:BoundField DataField="NUMEXT2" HeaderText="ORIGEN" />
                <asp:BoundField DataField="INIPERSUS" HeaderText="INICIO P" />
                <asp:BoundField DataField="FINPERSUS" HeaderText="FIN P" />
                <asp:BoundField DataField="TOTNUMLLA" HeaderText="PAQUETE LLAMADAS" />
                <asp:BoundField DataField="TOTLLAREA" HeaderText="LLAMADAS REA" />
                <asp:BoundField DataField="STATUS" HeaderText="ESTATUS" />
                <asp:BoundField DataField="orderhdr_id" HeaderText="ORDEN" />
                <asp:BoundField DataField="llave_unica" />
                <asp:ButtonField ButtonType="Button" Text="CAMBIAR" CommandName="select" />
            </Columns>
            <EditRowStyle BackColor="#999999" />
            <FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
            <HeaderStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
            <PagerStyle BackColor="#284775" ForeColor="White" HorizontalAlign="Center" />
            <RowStyle BackColor="#F7F6F3" ForeColor="#333333" />
            <SelectedRowStyle BackColor="#E2DED6" Font-Bold="True" ForeColor="#333333" />
            <SortedAscendingCellStyle BackColor="#E9E7E2" />
            <SortedAscendingHeaderStyle BackColor="#506C8C" />
            <SortedDescendingCellStyle BackColor="#FFFDF8" />
            <SortedDescendingHeaderStyle BackColor="#6F8DAE" />
        </asp:GridView>
    </div>

</asp:Content>
