﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Data.SqlClient;
using System.Data;


namespace WebApplication2
{
    public partial class WebForm5 : System.Web.UI.Page
    {

        public String SQL = "";
        public SqlDataReader rs;
        public SqlConnection myConnection;
        public SqlDataReader myReader;
        //your client id  
        string clientid = "143735854478-76v8cbc2nsdvddoklhra41eijdcgdm2j.apps.googleusercontent.com";
        //your client secret  
        string clientsecret = "A9el3hmHuyTxbshuDctaHLrI";
        //your redirection url          
        string redirection_url = "http://serviciowebidc.idconline.mx/CONSULTORIA/PSuccess";
        //string redirection_url = "http://localhost:50773/Success";
        string url = "https://accounts.google.com/o/oauth2/token";
        public class Tokenclass
        {
            public string access_token
            {
                get;
                set;
            }
            public string token_type
            {
                get;
                set;
            }
            public int expires_in
            {
                get;
                set;
            }
            public string refresh_token
            {
                get;
                set;
            }
        }
        public class Userclass
        {
            public string id
            {
                get;
                set;
            }
            public string name
            {
                get;
                set;
            }
            public string given_name
            {
                get;
                set;
            }
            public string family_name
            {
                get;
                set;
            }
            public string link
            {
                get;
                set;
            }
            public string picture
            {
                get;
                set;
            }
            public string gender
            {
                get;
                set;
            }
            public string locale
            {
                get;
                set;
            }
            public string email
            {
                get;
                set;
            }
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (Request.QueryString["code"] != null)
                {
                    GetToken(Request.QueryString["code"].ToString());
                }
                /*HttpCookie cookie = Request.Cookies.Get("UserID");
                if (cookie == null)
                {
                    Response.Redirect("http://serviciowebidc.idconline.mx/CONSULTORIA/Login");

                }
                else
                {
                    String data = Request.Cookies["UserID"].Value;
                    string[] words = data.Split('|');
                }*/
            }
        }
        public void GetToken(string code)
        {
            string poststring = "grant_type=authorization_code&code=" + code + "&client_id=" + clientid + "&client_secret=" + clientsecret + "&redirect_uri=" + redirection_url + "";
            var request = (HttpWebRequest)WebRequest.Create(url);
            request.ContentType = "application/x-www-form-urlencoded";
            request.Method = "POST";
            UTF8Encoding utfenc = new UTF8Encoding();
            byte[] bytes = utfenc.GetBytes(poststring);
            Stream outputstream = null;
            try
            {
                request.ContentLength = bytes.Length;
                outputstream = request.GetRequestStream();
                outputstream.Write(bytes, 0, bytes.Length);


                var response = (HttpWebResponse)request.GetResponse();
                var streamReader = new StreamReader(response.GetResponseStream());
                string responseFromServer = streamReader.ReadToEnd();
                JavaScriptSerializer js = new JavaScriptSerializer();
                Tokenclass obj = js.Deserialize<Tokenclass>(responseFromServer);
                GetuserProfile(obj.access_token);
            }
            catch { }
            //lo quite de aqui
        }
        public void GetuserProfile(string accesstoken)
        {
            string url = "https://www.googleapis.com/oauth2/v1/userinfo?alt=json&access_token=" + accesstoken + "";
            WebRequest request = WebRequest.Create(url);
            request.Credentials = CredentialCache.DefaultCredentials;
            WebResponse response = request.GetResponse();
            Stream dataStream = response.GetResponseStream();
            StreamReader reader = new StreamReader(dataStream);
            string responseFromServer = reader.ReadToEnd();
            reader.Close();
            response.Close();
            JavaScriptSerializer js = new JavaScriptSerializer();
            Userclass userinfo = js.Deserialize<Userclass>(responseFromServer);
            imgprofile.ImageUrl = userinfo.picture;
            lblid.Text = userinfo.id;
            lblgender.Text = userinfo.email;
            lbllocale.Text = userinfo.locale;
            lblname.Text = userinfo.name;
            hylprofile.NavigateUrl = userinfo.link;



            ValidarUsuario(userinfo.email, userinfo.given_name, userinfo.picture);

        }


        protected void Page_PreRender(object sender, EventArgs e)
        {
            if (myConnection != null)
            {
                if ((int)myConnection.State == 1)
                {
                    myConnection.Close();
                }
            }
        }
        public void libera()
        {
            if (myReader != null)
            {
                myReader.Dispose();
            }

            if (myConnection != null)
            {
                myConnection.Close();
            }


        }

        public SqlDataReader conex(string qry)
        {
            //recuperamos la conexion del config
            Configuration rootWebConfig = ConfigurationManager.OpenExeConfiguration("");
            ConnectionStringSettings connString;
            //verifica que exista al menos una cadena de conexión
            if (0 < rootWebConfig.ConnectionStrings.ConnectionStrings.Count)
            {
                //lee la cadena de conexión
                connString = rootWebConfig.ConnectionStrings.ConnectionStrings["ConexionAcceso"];
                //verifica que exista la cadena de conexión especificada
                if (null != connString)
                {
                    //crea la cadena de conexión

                    //abre la conexión
                    try
                    {
                        if (myConnection == null)
                        {
                            myConnection = new SqlConnection(connString.ConnectionString);
                            myConnection.Open();
                        }
                        //ejecuta el comando
                        try
                        {
                            SqlCommand myCommand = new SqlCommand(qry, myConnection);
                            myCommand.CommandTimeout = 5000000;
                            if (myReader != null)
                            {
                                myReader.Dispose();
                            }
                            myReader = myCommand.ExecuteReader();
                        }
                        catch (Exception e)
                        {
                            throw new System.ArgumentException("conn error", e);
                        }
                    }
                    catch (Exception e)
                    {
                        throw new System.ArgumentException("db conn error 1", e);
                    }
                    //finally { myConnection.Close(); }
                    return myReader;
                }
                else
                    throw new System.ArgumentException("ConnString is null", "original");
            }
            else
                throw new System.ArgumentException("ConnStrings is empty", "original");

        }
        public void ValidarUsuario(String a, String b, String c)
        {
            SQL = "select * from fac0mxdb..Fac_SegLogin where email='" + a + "' and rol='CONSULTORIA'";
            rs = conex(SQL);
            if (rs.HasRows)
            {
                rs.Read();
                HttpCookie userIdCookie = new HttpCookie("UserID");
                userIdCookie.Value = a + "|" + b + "|" + c;
                Response.Cookies.Add(userIdCookie);
                //Response.Redirect("http://serviciowebidc.idconline.mx/CONSULTORIA/Default");
                Label1.Text = "Se encontro en base";

            }
            else
            {
                Label1.Text = "SIN PERMISOS";
            }
            libera();
        }
    }
}