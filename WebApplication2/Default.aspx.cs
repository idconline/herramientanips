﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Data.SqlClient;
using System.Data;

namespace WebApplication2
{
    public partial class _Default : Page
    {
        public String SQL = "";
        public SqlDataReader rs;
        public SqlDataReader rs2;
        public SqlConnection myConnection;
        public SqlDataReader myReader;
        public SqlConnection myConnection2;
        public SqlDataReader myReader2;
        public DataTable dt = new DataTable();
        protected void Page_Load(object sender, EventArgs e)
        {
            DivAlert.Visible = false;
            Label1.Text = "";            
        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            String a = "";
            int b = 0;
            int status_nip = 0;
            int x = 0;

            //limpiamos los datos
            Table1.Visible = false;
            Label2.Text = "";
            Label6.Text = "";
            Label3.Text = "";
            Label7.Text = "";
            Label4.Text = "";
            Label8.Text = "";
            Label5.Text = "";
            Label9.Text = "";
            Label10.Text = "";
            Label11.Text = "";
            Label12.Text = "";
            GridView1.DataSource = "";
            GridView1.DataBind();


            if (TextBox1.Text.Length > 0)
            {
                a = TextBox1.Text;
                b = Convert.ToInt16(DropDownList1.SelectedValue.ToString());
                int duplicado = 0;

                if (b == 1)
                {
                    SQL = "select COUNT(*) from fac0mxdb..CLIENTES where NIP=" + a;
                    rs = conex(SQL);
                    if (rs.HasRows)
                    {
                        rs.Read();
                        duplicado = Convert.ToInt16(rs[0].ToString());
                    }

                    switch (duplicado)
                    {
                        case 0:
                            DivAlert.Attributes["class"] = "alert alert-secondary";
                            DivAlert.Visible = true;
                            Label1.Text = "No encontrado";
                            break;
                        case 1:
                            Label1.Text = "";
                            SQL = "select (TOTNUMLLA-TOTLLAREA) ,* from fac0mxdb..CLIENTES where NIP=" + a;
                            rs = conex(SQL);
                            if (rs.HasRows)
                            {
                                rs.Read();
                                status_nip = Convert.ToInt16(rs["STATUS"].ToString());
                                x = Convert.ToInt16(rs[0].ToString());
                            }
                            if (status_nip != 1 || x <= 0)
                            {
                                DivAlert.Attributes["class"] = "alert alert-warning";
                                DivAlert.Visible = true;
                                Label1.Text = "<b>El NIP esta desactivado o supero su numero de llamdas disponibles</b>";

                                //Label1.Text = "<b>El NIP esta activo y le quedan <b>" + x.ToString() + "</b> llamadas disponibles</b>";


                                Table1.Visible = true;
                                Label2.Text = rs[4].ToString();//empresa
                                Label6.Text = "<b>" + rs[1].ToString() + "</b>";//nip
                                Label3.Text = rs[3].ToString();//suscriptor
                                Label7.Text = rs[2].ToString();//no cliente
                                Label4.Text = rs[10].ToString();//telefono
                                Label8.Text = rs[16].ToString() + " <b>/</b> " + rs[17].ToString();//periodo
                                Label5.Text = rs[23].ToString();//correo
                                if (status_nip == 1)
                                    Label9.Text = "ACTIVO";//estatus
                                else
                                    Label9.Text = "DESACTIVADO";//estatus

                                Label10.Text = rs[18].ToString();//adquiridas
                                Label11.Text = rs[19].ToString();//realizadas
                                Label12.Text = rs[0].ToString();//disponibles
                                HyperLink1.NavigateUrl = "Detalle.aspx?nip=" + a;

                            }
                            else
                            {


                                DivAlert.Attributes["class"] = "alert alert-success";
                                DivAlert.Visible = true;
                                Label1.Text = "<b>El NIP esta activo y le quedan <b>" + x.ToString() + "</b> llamadas disponibles</b>";


                                Table1.Visible = true;
                                Label2.Text = rs[4].ToString();//empresa
                                Label6.Text = "<b>" + rs[1].ToString() + "</b>";//nip
                                Label3.Text = rs[3].ToString();//suscriptor
                                Label7.Text = rs[2].ToString();//no cliente
                                Label4.Text = rs[10].ToString();//telefono
                                Label8.Text = rs[16].ToString() + " <b>/</b> " + rs[17].ToString();//periodo
                                Label5.Text = rs[23].ToString();//correo
                                if (status_nip == 1)
                                    Label9.Text = "ACTIVO";//estatus
                                else
                                    Label9.Text = "DESACTIVADO";//estatus

                                Label10.Text = rs[18].ToString();//adquiridas
                                Label11.Text = rs[19].ToString();//realizadas
                                Label12.Text = rs[0].ToString();//disponibles

                                HyperLink1.NavigateUrl = "Detalle.aspx?nip=" + a;

                            }

                            break;
                        default:
                            DivAlert.Attributes["class"] = "alert alert-danger";
                            DivAlert.Visible = true;
                            Label1.Text = "Duplicado se encontraron " + duplicado.ToString() + " registros con este NIP";

                            Table1.Visible = false;

                            dt.Columns.AddRange(new DataColumn[13] {
                            new DataColumn("NIP", typeof(String)),
                            new DataColumn("NUMSUS", typeof(String)),
                            new DataColumn("NOMSUS", typeof(String)),
                            new DataColumn("NOMEMP", typeof(String)),
                            new DataColumn("NUMTELE1", typeof(String)),
                            new DataColumn("NUMEXT2", typeof(String)),
                            new DataColumn("INIPERSUS", typeof(String)),
                            new DataColumn("FINPERSUS", typeof(String)),
                            new DataColumn("TOTNUMLLA", typeof(String)),
                            new DataColumn("TOTLLAREA", typeof(String)),
                            new DataColumn("STATUS", typeof(String)),
                            new DataColumn("orderhdr_id", typeof(String)),
                            new DataColumn("llave_unica", typeof(String))
                        });

                            SQL = "select * from fac0mxdb..CLIENTES where NIP=" + a;
                            rs = conex(SQL);
                            if (rs.HasRows)
                            {
                                while (rs.Read())
                                {
                                    // Response.Write("<script LANGUAGE='JavaScript' >alert('" + rs[2].ToString() + "')</script>");
                                    dt.Rows.Add(rs["NIP"].ToString(), rs["NUMSUS"].ToString(), rs["NOMSUS"].ToString(), rs["NOMEMP"].ToString(), rs["NUMTELE1"].ToString(), rs["NUMEXT2"].ToString(), rs["INIPERSUS"].ToString(), rs["FINPERSUS"].ToString(), rs["TOTNUMLLA"].ToString(), rs["TOTLLAREA"].ToString(), rs["STATUS"].ToString(), rs["orderhdr_id"].ToString(), rs["llave_unica"].ToString());
                                }
                            }
                            //libera();
                            GridView1.DataSource = dt;
                            GridView1.DataBind();

                            break;

                    }


                    libera();

                }
                else if (b == 2)
                {
                    SQL = "select COUNT(*) from fac0mxdb..CLIENTES where email='" + a + "'" ;
                    rs = conex(SQL);
                    if (rs.HasRows)
                    {
                        rs.Read();
                        duplicado = Convert.ToInt16(rs[0].ToString());
                    }
                    switch (duplicado)
                    {
                        case 0:
                            DivAlert.Attributes["class"] = "alert alert-secondary";
                            DivAlert.Visible = true;
                            Label1.Text = "No encontrado";
                            break;
                        case 1:
                            Label1.Text = "";
                            SQL = "select (TOTNUMLLA-TOTLLAREA) ,* from fac0mxdb..CLIENTES where email='" + a + "'";
                            rs = conex(SQL);
                            if (rs.HasRows)
                            {
                                rs.Read();
                                status_nip = Convert.ToInt16(rs["STATUS"].ToString());
                                x = Convert.ToInt16(rs[0].ToString());
                            }
                            if (status_nip != 1 || x <= 0)
                            {
                                DivAlert.Attributes["class"] = "alert alert-warning";
                                DivAlert.Visible = true;
                                Label1.Text = "<b>El NIP asociado al Email esta desactivado o supero su numero de llamdas disponibles</b>";

                                //Label1.Text = "<b>El NIP esta activo y le quedan <b>" + x.ToString() + "</b> llamadas disponibles</b>";


                                Table1.Visible = true;
                                Label2.Text = rs[4].ToString();//empresa
                                Label6.Text = "<b>" + rs[1].ToString() + "</b>";//nip
                                Label3.Text = rs[3].ToString();//suscriptor
                                Label7.Text = rs[2].ToString();//no cliente
                                Label4.Text = rs[10].ToString();//telefono
                                Label8.Text = rs[16].ToString() + " <b>/</b> " + rs[17].ToString();//periodo
                                Label5.Text = rs[23].ToString();//correo
                                if (status_nip == 1)
                                    Label9.Text = "ACTIVO";//estatus
                                else
                                    Label9.Text = "DESACTIVADO";//estatus

                                Label10.Text = rs[18].ToString();//adquiridas
                                Label11.Text = rs[19].ToString();//realizadas
                                Label12.Text = rs[0].ToString();//disponibles
                                HyperLink1.NavigateUrl = "Detalle.aspx?nip=" + rs["NIP"].ToString();

                            }
                            else
                            {


                                DivAlert.Attributes["class"] = "alert alert-success";
                                DivAlert.Visible = true;
                                Label1.Text = "<b>El NIP  asociado al Email esta activo y le quedan <b>" + x.ToString() + "</b> llamadas disponibles</b>";


                                Table1.Visible = true;
                                Label2.Text = rs[4].ToString();//empresa
                                Label6.Text = "<b>" + rs[1].ToString() + "</b>";//nip
                                Label3.Text = rs[3].ToString();//suscriptor
                                Label7.Text = rs[2].ToString();//no cliente
                                Label4.Text = rs[10].ToString();//telefono
                                Label8.Text = rs[16].ToString() + " <b>/</b> " + rs[17].ToString();//periodo
                                Label5.Text = rs[23].ToString();//correo
                                if (status_nip == 1)
                                    Label9.Text = "ACTIVO";//estatus
                                else
                                    Label9.Text = "DESACTIVADO";//estatus

                                Label10.Text = rs[18].ToString();//adquiridas
                                Label11.Text = rs[19].ToString();//realizadas
                                Label12.Text = rs[0].ToString();//disponibles

                                HyperLink1.NavigateUrl = "Detalle.aspx?nip=" + rs["NIP"].ToString();

                            }

                            break;
                        default:
                            DivAlert.Attributes["class"] = "alert alert-danger";
                            DivAlert.Visible = true;
                            Label1.Text = "Duplicado se encontraron " + duplicado.ToString() + " registros con este EMAIL";

                            Table1.Visible = false;

                            dt.Columns.AddRange(new DataColumn[12] {
                            new DataColumn("NIP", typeof(String)),
                            new DataColumn("NUMSUS", typeof(String)),
                            new DataColumn("NOMSUS", typeof(String)),
                            new DataColumn("NOMEMP", typeof(String)),
                            new DataColumn("NUMTELE1", typeof(String)),
                            new DataColumn("NUMEXT2", typeof(String)),
                            new DataColumn("INIPERSUS", typeof(String)),
                            new DataColumn("FINPERSUS", typeof(String)),
                            new DataColumn("TOTNUMLLA", typeof(String)),
                            new DataColumn("TOTLLAREA", typeof(String)),
                            new DataColumn("STATUS", typeof(String)),
                            new DataColumn("orderhdr_id", typeof(String))
                        });

                            SQL = "select * from fac0mxdb..CLIENTES where email='" + a + "'" ;
                            rs = conex(SQL);
                            if (rs.HasRows)
                            {
                                while (rs.Read())
                                {
                                    // Response.Write("<script LANGUAGE='JavaScript' >alert('" + rs[2].ToString() + "')</script>");
                                    dt.Rows.Add(rs["NIP"].ToString(), rs["NUMSUS"].ToString(), rs["NOMSUS"].ToString(), rs["NOMEMP"].ToString(), rs["NUMTELE1"].ToString(), rs["NUMEXT2"].ToString(), rs["INIPERSUS"].ToString(), rs["FINPERSUS"].ToString(), rs["TOTNUMLLA"].ToString(), rs["TOTLLAREA"].ToString(), rs["STATUS"].ToString(), rs["orderhdr_id"].ToString());
                                }
                            }
                            //libera();
                            GridView1.DataSource = dt;
                            GridView1.DataBind();

                            break;

                    }


                    libera();


                }
                else
                {
                    SQL = "select COUNT(*) from fac0mxdb..CLIENTES where NUMSUS=" + a;
                    rs = conex(SQL);
                    if (rs.HasRows)
                    {
                        rs.Read();
                        duplicado = Convert.ToInt16(rs[0].ToString());
                    }

                    switch (duplicado)
                    {
                        case 0:
                            DivAlert.Attributes["class"] = "alert alert-secondary";
                            DivAlert.Visible = true;
                            Label1.Text = "No encontrado";
                            break;
                        case 1:
                            Label1.Text = "";
                            SQL = "select (TOTNUMLLA-TOTLLAREA) ,* from fac0mxdb..CLIENTES where NUMSUS=" + a;
                            rs = conex(SQL);
                            if (rs.HasRows)
                            {
                                rs.Read();
                                status_nip = Convert.ToInt16(rs["STATUS"].ToString());
                                x = Convert.ToInt16(rs[0].ToString());
                            }
                            if (status_nip != 1 || x <= 0)
                            {
                                DivAlert.Attributes["class"] = "alert alert-warning";
                                DivAlert.Visible = true;
                                Label1.Text = "<b>El NIP asociado al Numero de cliente esta desactivado o supero su numero de llamdas disponibles</b>";

                                //Label1.Text = "<b>El NIP esta activo y le quedan <b>" + x.ToString() + "</b> llamadas disponibles</b>";


                                Table1.Visible = true;
                                Label2.Text = rs[4].ToString();//empresa
                                Label6.Text = "<b>" + rs[1].ToString() + "</b>";//nip
                                Label3.Text = rs[3].ToString();//suscriptor
                                Label7.Text = rs[2].ToString();//no cliente
                                Label4.Text = rs[10].ToString();//telefono
                                Label8.Text = rs[16].ToString() + " <b>/</b> " + rs[17].ToString();//periodo
                                Label5.Text = rs[23].ToString();//correo
                                if (status_nip == 1)
                                    Label9.Text = "ACTIVO";//estatus
                                else
                                    Label9.Text = "DESACTIVADO";//estatus

                                Label10.Text = rs[18].ToString();//adquiridas
                                Label11.Text = rs[19].ToString();//realizadas
                                Label12.Text = rs[0].ToString();//disponibles
                                HyperLink1.NavigateUrl = "Detalle.aspx?nip=" + rs["NIP"].ToString();

                            }
                            else
                            {


                                DivAlert.Attributes["class"] = "alert alert-success";
                                DivAlert.Visible = true;
                                Label1.Text = "<b>El NIP esta activo y le quedan <b>" + x.ToString() + "</b> llamadas disponibles</b>";


                                Table1.Visible = true;
                                Label2.Text = rs[4].ToString();//empresa
                                Label6.Text = "<b>" + rs[1].ToString() + "</b>";//nip
                                Label3.Text = rs[3].ToString();//suscriptor
                                Label7.Text = rs[2].ToString();//no cliente
                                Label4.Text = rs[10].ToString();//telefono
                                Label8.Text = rs[16].ToString() + " <b>/</b> " + rs[17].ToString();//periodo
                                Label5.Text = rs[23].ToString();//correo
                                if (status_nip == 1)
                                    Label9.Text = "ACTIVO";//estatus
                                else
                                    Label9.Text = "DESACTIVADO";//estatus

                                Label10.Text = rs[18].ToString();//adquiridas
                                Label11.Text = rs[19].ToString();//realizadas
                                Label12.Text = rs[0].ToString();//disponibles

                                HyperLink1.NavigateUrl = "Detalle.aspx?nip=" + rs["NIP"].ToString();

                            }

                            break;
                        default:
                            DivAlert.Attributes["class"] = "alert alert-danger";
                            DivAlert.Visible = true;
                            Label1.Text = "Duplicado se encontraron " + duplicado.ToString() + " registros con este NUMERO DE CLIENTE ";

                            Table1.Visible = false;

                            dt.Columns.AddRange(new DataColumn[12] {
                            new DataColumn("NIP", typeof(String)),
                            new DataColumn("NUMSUS", typeof(String)),
                            new DataColumn("NOMSUS", typeof(String)),
                            new DataColumn("NOMEMP", typeof(String)),
                            new DataColumn("NUMTELE1", typeof(String)),
                            new DataColumn("NUMEXT2", typeof(String)),
                            new DataColumn("INIPERSUS", typeof(String)),
                            new DataColumn("FINPERSUS", typeof(String)),
                            new DataColumn("TOTNUMLLA", typeof(String)),
                            new DataColumn("TOTLLAREA", typeof(String)),
                            new DataColumn("STATUS", typeof(String)),
                            new DataColumn("orderhdr_id", typeof(String))
                        });

                            SQL = "select * from fac0mxdb..CLIENTES where NUMSUS=" + a;
                            rs = conex(SQL);
                            if (rs.HasRows)
                            {
                                while (rs.Read())
                                {
                                    // Response.Write("<script LANGUAGE='JavaScript' >alert('" + rs[2].ToString() + "')</script>");
                                    dt.Rows.Add(rs["NIP"].ToString(), rs["NUMSUS"].ToString(), rs["NOMSUS"].ToString(), rs["NOMEMP"].ToString(), rs["NUMTELE1"].ToString(), rs["NUMEXT2"].ToString(), rs["INIPERSUS"].ToString(), rs["FINPERSUS"].ToString(), rs["TOTNUMLLA"].ToString(), rs["TOTLLAREA"].ToString(), rs["STATUS"].ToString(), rs["orderhdr_id"].ToString());
                                }
                            }
                            //libera();
                            GridView1.DataSource = dt;
                            GridView1.DataBind();

                            break;

                    }


                    libera();
                }


            }


            else
            {
                Response.Write("<script LANGUAGE='JavaScript' >alert('favor de colocar algun dato para buscar')</script>");
            }


           
        }

        protected void Page_PreRender(object sender, EventArgs e)
        {
            if (myConnection != null)
            {
                if ((int)myConnection.State == 1)
                {
                    myConnection.Close();
                }
            }

            if (myConnection2 != null)
            {
                if ((int)myConnection2.State == 1)
                {
                    myConnection2.Close();
                }
            }
        }

        public SqlDataReader conex(string qry)
        {
            //recuperamos la conexion del config
            Configuration rootWebConfig = ConfigurationManager.OpenExeConfiguration("");
            ConnectionStringSettings connString;
            //verifica que exista al menos una cadena de conexión
            if (0 < rootWebConfig.ConnectionStrings.ConnectionStrings.Count)
            {
                //lee la cadena de conexión
                connString = rootWebConfig.ConnectionStrings.ConnectionStrings["ConexionAcceso"];
                //verifica que exista la cadena de conexión especificada
                if (null != connString)
                {
                    //crea la cadena de conexión

                    //abre la conexión
                    try
                    {
                        if (myConnection == null)
                        {
                            myConnection = new SqlConnection(connString.ConnectionString);
                            myConnection.Open();
                        }
                        //ejecuta el comando
                        try
                        {
                            SqlCommand myCommand = new SqlCommand(qry, myConnection);
                            myCommand.CommandTimeout = 5000000;
                            if (myReader != null)
                            {
                                myReader.Dispose();
                            }
                            myReader = myCommand.ExecuteReader();
                        }
                        catch (Exception e)
                        {
                            throw new System.ArgumentException("conn error", e);
                        }
                    }
                    catch (Exception e)
                    {
                        throw new System.ArgumentException("db conn error 1", e);
                    }
                    //finally { myConnection.Close(); }
                    return myReader;
                }
                else
                    throw new System.ArgumentException("ConnString is null", "original");
            }
            else
                throw new System.ArgumentException("ConnStrings is empty", "original");

        }

        public SqlDataReader conex3(string qry)
        {
            //recuperamos la conexion del config
            Configuration rootWebConfig = ConfigurationManager.OpenExeConfiguration("");
            ConnectionStringSettings connString;
            //verifica que exista al menos una cadena de conexión
            if (0 < rootWebConfig.ConnectionStrings.ConnectionStrings.Count)
            {
                //lee la cadena de conexión
                connString = rootWebConfig.ConnectionStrings.ConnectionStrings["ConexionAcceso"];
                //verifica que exista la cadena de conexión especificada
                if (null != connString)
                {
                    //crea la cadena de conexión

                    //abre la conexión
                    try
                    {
                        if (myConnection2 == null)
                        {
                            myConnection2 = new SqlConnection(connString.ConnectionString);
                            myConnection2.Open();
                        }
                        //ejecuta el comando
                        try
                        {
                            SqlCommand myCommand = new SqlCommand(qry, myConnection2);
                            myCommand.CommandTimeout = 5000000;
                            if (myReader2 != null)
                            {
                                myReader2.Dispose();
                            }
                            myReader2 = myCommand.ExecuteReader();
                        }
                        catch (Exception e)
                        {
                            throw new System.ArgumentException("conn error", e);
                        }
                    }
                    catch (Exception e)
                    {
                        throw new System.ArgumentException("db conn error 1", e);
                    }
                    //finally { myConnection.Close(); }
                    return myReader2;
                }
                else
                    throw new System.ArgumentException("ConnString is null", "original");
            }
            else
                throw new System.ArgumentException("ConnStrings is empty", "original");

        }

        //escritura
        public SqlDataReader conex2(string qry)
        {
            SqlDataReader myReader = null;
            Configuration rootConfig = ConfigurationManager.OpenExeConfiguration("");
            ConnectionStringSettings connString;
            if (0 < rootConfig.ConnectionStrings.ConnectionStrings.Count)
            {
                connString = rootConfig.ConnectionStrings.ConnectionStrings["ConexionAcceso"];
                if (null != connString)
                {
                    SqlConnection myConnection2 = new SqlConnection(connString.ConnectionString);
                    try
                    {
                        myConnection2.Open();
                        try
                        {
                            SqlCommand myCommand = new SqlCommand(qry, myConnection2);
                            //myCommand.CommandTimeout = 5000;
                            myCommand.ExecuteNonQuery();
                        }
                        catch (Exception e)
                        {
                            throw new System.ArgumentException("conn error", e);
                        }
                    }
                    catch (Exception e)
                    {
                        throw new System.ArgumentException("db conn error 1", e);
                    }
                    //finally { myConnection2.Close(); }
                    return myReader;
                }
                else
                    throw new System.ArgumentException("ConnString is null", "original");
            }
            else
                throw new System.ArgumentException("ConnStrings is empty", "original");
        }

        public void libera()
        {
            if (myReader != null)
            {
                myReader.Dispose();
            }
            if (myReader2 != null)
            {
                myReader2.Dispose();
            }
            if (myConnection != null)
            {
                myConnection.Close();
            }
            if (myConnection2 != null)
            {
                myConnection2.Close();
            }

        }

        public String GenerateRandom()
        {
            System.Random randomGenerate = new System.Random();
            System.String sPassword = "";
            sPassword = System.Convert.ToString(randomGenerate.Next(00000001, 99999999));
            return sPassword.Substring(sPassword.Length - 6, 6);
        }

      

        protected void GridView1_SelectedIndexChanged(object sender, EventArgs e)
        {
            GridViewRow row = GridView1.SelectedRow;

            Response.Redirect("~/Contact?llave=" + row.Cells[12].Text);

            //Response.Write("<script LANGUAGE='JavaScript' >alert('es' " + row.Cells[12].Text +")</script>");


        }
    }
}