﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Data.SqlClient;
using System.Data;
using ResponseModels;
using sandbox;

namespace WebApplication2
{
    public partial class Contact : Page
    {
        public String SQL = "";
        public SqlDataReader rs;
        public SqlDataReader rs2;
        public SqlConnection myConnection;
        public SqlDataReader myReader;
        public SqlConnection myConnection2;
        public SqlDataReader myReader2;
        public DataTable dt = new DataTable();
        public int new_nip_gen = 0;

        public SqlDataReader conex(string qry)
        {
            //recuperamos la conexion del config
            Configuration rootWebConfig = ConfigurationManager.OpenExeConfiguration("");
            ConnectionStringSettings connString;
            //verifica que exista al menos una cadena de conexión
            if (0 < rootWebConfig.ConnectionStrings.ConnectionStrings.Count)
            {
                //lee la cadena de conexión
                connString = rootWebConfig.ConnectionStrings.ConnectionStrings["ConexionAcceso"];
                //verifica que exista la cadena de conexión especificada
                if (null != connString)
                {
                    //crea la cadena de conexión

                    //abre la conexión
                    try
                    {
                        if (myConnection == null)
                        {
                            myConnection = new SqlConnection(connString.ConnectionString);
                            myConnection.Open();
                        }
                        //ejecuta el comando
                        try
                        {
                            SqlCommand myCommand = new SqlCommand(qry, myConnection);
                            myCommand.CommandTimeout = 5000000;
                            if (myReader != null)
                            {
                                myReader.Dispose();
                            }
                            myReader = myCommand.ExecuteReader();
                        }
                        catch (Exception e)
                        {
                            throw new System.ArgumentException("conn error", e);
                        }
                    }
                    catch (Exception e)
                    {
                        throw new System.ArgumentException("db conn error 1", e);
                    }
                    //finally { myConnection.Close(); }
                    return myReader;
                }
                else
                    throw new System.ArgumentException("ConnString is null", "original");
            }
            else
                throw new System.ArgumentException("ConnStrings is empty", "original");

        }

        public SqlDataReader conex3(string qry)
        {
            //recuperamos la conexion del config
            Configuration rootWebConfig = ConfigurationManager.OpenExeConfiguration("");
            ConnectionStringSettings connString;
            //verifica que exista al menos una cadena de conexión
            if (0 < rootWebConfig.ConnectionStrings.ConnectionStrings.Count)
            {
                //lee la cadena de conexión
                connString = rootWebConfig.ConnectionStrings.ConnectionStrings["ConexionAcceso"];
                //verifica que exista la cadena de conexión especificada
                if (null != connString)
                {
                    //crea la cadena de conexión

                    //abre la conexión
                    try
                    {
                        if (myConnection2 == null)
                        {
                            myConnection2 = new SqlConnection(connString.ConnectionString);
                            myConnection2.Open();
                        }
                        //ejecuta el comando
                        try
                        {
                            SqlCommand myCommand = new SqlCommand(qry, myConnection2);
                            myCommand.CommandTimeout = 5000000;
                            if (myReader2 != null)
                            {
                                myReader2.Dispose();
                            }
                            myReader2 = myCommand.ExecuteReader();
                        }
                        catch (Exception e)
                        {
                            throw new System.ArgumentException("conn error", e);
                        }
                    }
                    catch (Exception e)
                    {
                        throw new System.ArgumentException("db conn error 1", e);
                    }
                    //finally { myConnection.Close(); }
                    return myReader2;
                }
                else
                    throw new System.ArgumentException("ConnString is null", "original");
            }
            else
                throw new System.ArgumentException("ConnStrings is empty", "original");

        }

        //escritura
        public SqlDataReader conex2(string qry)
        {
            SqlDataReader myReader = null;
            Configuration rootConfig = ConfigurationManager.OpenExeConfiguration("");
            ConnectionStringSettings connString;
            if (0 < rootConfig.ConnectionStrings.ConnectionStrings.Count)
            {
                connString = rootConfig.ConnectionStrings.ConnectionStrings["ConexionAcceso"];
                if (null != connString)
                {
                    SqlConnection myConnection2 = new SqlConnection(connString.ConnectionString);
                    try
                    {
                        myConnection2.Open();
                        try
                        {
                            SqlCommand myCommand = new SqlCommand(qry, myConnection2);
                            //myCommand.CommandTimeout = 5000;
                            myCommand.ExecuteNonQuery();
                        }
                        catch (Exception e)
                        {
                            throw new System.ArgumentException("conn error", e);
                        }
                    }
                    catch (Exception e)
                    {
                        throw new System.ArgumentException("db conn error 1", e);
                    }
                    //finally { myConnection2.Close(); }
                    return myReader;
                }
                else
                    throw new System.ArgumentException("ConnString is null", "original");
            }
            else
                throw new System.ArgumentException("ConnStrings is empty", "original");
        }

        public void libera()
        {
            if (myReader != null)
            {
                myReader.Dispose();
            }
            if (myReader2 != null)
            {
                myReader2.Dispose();
            }
            if (myConnection != null)
            {
                myConnection.Close();
            }
            if (myConnection2 != null)
            {
                myConnection2.Close();
            }

        }
        protected void Page_PreRender(object sender, EventArgs e)
        {
            if (myConnection != null)
            {
                if ((int)myConnection.State == 1)
                {
                    myConnection.Close();
                }
            }

            if (myConnection2 != null)
            {
                if ((int)myConnection2.State == 1)
                {
                    myConnection2.Close();
                }
            }
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                //Response.Write("<script LANGUAGE='JavaScript' >alert('asd')</script>");

                if (Request.QueryString["llave"] != null)
                {
                    
                    string val1 = Request.QueryString["llave"];
                    TextBox9.Text = val1;
                    //Response.Write("<script LANGUAGE='JavaScript' >alert('" + val1 + "')</script>");
                    SQL = "select * from fac0mxdb..CLIENTES where llave_unica='" + val1 + "'";
                    rs = conex(SQL);
                    if (rs.HasRows)
                    {
                        rs.Read();
                        TextBox1.Text= rs["NIP"].ToString();
                    }

                        //datoos_llamadas(val1);
                        Button1.Enabled = true;
                        Button2.Enabled = true;
                }
                else
                {

                }
            }
            else
            {
                //Response.Write("<script LANGUAGE='JavaScript' >alert('xxx')</script>");
            }

        }

        public String GenerateRandom()
        {
            System.Random randomGenerate = new System.Random();
            System.String sPassword = "";
            sPassword = System.Convert.ToString(randomGenerate.Next(00000001, 99999999));
            return sPassword.Substring(sPassword.Length - 6, 6);
        }

        public bool valida_nip_nuevo()
        {
            new_nip_gen = Convert.ToInt32(GenerateRandom());
            bool banderaE = false;

            SQL = "SELECT * from eclipse_produccion..expan_nip where nip=" + new_nip_gen;
            rs2 = conex3(SQL);
            if (rs2.HasRows)
            {
                rs2.Read();
                banderaE = true;

            }
            return banderaE;
        }

        protected void Button1_Click1(object sender, EventArgs e)
        {
            String a1, a2, a3, a4, a5;
            int Order_odoo = 0;
            String x1="0", x2="0";


            a1 = TextBox1.Text;//nip actual
            a2 = TextBox2.Text;//nip nuevo
            a3 = DropDownList1.SelectedValue.ToString();//ejecutivo
            a4 = TextBox3.Text;//notivo
            a5 = TextBox9.Text;//key;
            if(a1.Length>0 && a2.Length>0 && a3.Length>0 && a4.Length>0 && a5.Length > 0)
            {

                SQL = "select order_odoo from BitOdooNIP where nip=='" + a5 + "'";
                rs = conex(SQL);
                if (rs.HasRows)
                {
                    rs.Read();
                    Order_odoo =int.Parse( rs["order_odoo"].ToString());
                  
                }


                SQL = "INSERT INTO fac0mxdb..BIT_CAMBIO_NIP VALUES ('"+a1+ "','" + a2 + "','" + a3 + "','" + a5 + "','" + a4 + "',GETDATE(),'')";
                conex2(SQL);

                SQL = "update fac0mxdb..CLIENTES set nip="+ a2 + " where llave_unica='" + a5 + "'";
                conex2(SQL);

                SQL = "select * from fac0mxdb..CLIENTES where llave_unica='" + a5 + "'";
                rs = conex(SQL);
                if (rs.HasRows)
                {
                    rs.Read();
                    x1 = rs["orderhdr_id"].ToString();
                    x2 = rs["NUMSUS"].ToString();                    
                }

                SQL = "update eclipse_produccion..expan_nip set nip=" + a2 + " where orderhdr_id="+ x1 +" and customer_id="+x2+" and nip="+a1;
                conex2(SQL);
                    


                Response.Write("<script LANGUAGE='JavaScript' >alert('EL CAMBIO SE REALIZO ')</script>");

               

                OperationSand ChangeNip = new OperationSand();


                bool encontrados = ChangeNip.UpdateOdooNip(Order_odoo,int.Parse( a2), int.Parse(a1));
                


            }
            else
            {
                Response.Write("<script LANGUAGE='JavaScript' >alert('Se deben llenar todos los campos')</script>");
            }

        }

        protected void Button2_Click1(object sender, EventArgs e)
        {
            for (int i = 0; i < 5; i++)
            {                
                if (valida_nip_nuevo() == false)
                {
                    i = 6;                    
                    TextBox2.Text = new_nip_gen.ToString();
                }
                else
                {
                    i = 1;
                }
            }
            
        }
    }
}