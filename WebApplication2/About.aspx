﻿<%@ Page Title="About" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="About.aspx.cs" Inherits="WebApplication2.About" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <div class="">
        <h1>CONSULTORIA IDC-DESCONTAR LLAMADAS MANUALES</h1>               
    </div>
    <br />
    <br />
    <asp:Label ID="Label1" runat="server" Text="<strong>ADVERTENCIA</strong> Esto descontara el numero de llamadas indicadas al suscriptor es un proceso que no se puede deshacer" CssClass="alert alert-warning"></asp:Label>
    <br />
    <br />
    <asp:Table ID="Table1" runat="server" Visible="true" GridLines="Horizontal" Caption="DATOS" CssClass="table table-striped">
        <asp:TableRow>
            <asp:TableHeaderCell>NIP:</asp:TableHeaderCell>
            <asp:TableCell>   <asp:TextBox ID="TextBox1" runat="server"  CssClass="form-control" ></asp:TextBox> </asp:TableCell>
            <asp:TableHeaderCell>TEMA:</asp:TableHeaderCell>
            <asp:TableCell>
                <asp:TextBox ID="TextBox2" runat="server"  CssClass="form-control" ></asp:TextBox></asp:TableCell>
        </asp:TableRow>
        <asp:TableRow>
            <asp:TableHeaderCell>CONSULTOR:</asp:TableHeaderCell>
            <asp:TableCell> <asp:DropDownList ID="DropDownList1" runat="server"  CssClass="form-control" >
                <asp:ListItem Value="xxx">Seleccione un consultor</asp:ListItem>
                <asp:ListItem Value="acruzh">Ana Jazmin Cruz Hernandez</asp:ListItem>
                <asp:ListItem Value="apaniaguam">Angeles Paniagua Marquez</asp:ListItem>
                <asp:ListItem Value="acastillos">Antonio Castillo Sanchez</asp:ListItem>
                <asp:ListItem Value="egonzalez">Elizabeth Gonzalez</asp:ListItem>
                <asp:ListItem Value="eriverar">Erika Rivera Romo</asp:ListItem>
                <asp:ListItem Value="esanciprianp">Ernesto Sanciprian Pacheco</asp:ListItem>
                <asp:ListItem Value="fbritom">Francisco Brito MArquez</asp:ListItem>
                <asp:ListItem Value="evegar">Irene Vega Rivera</asp:ListItem>
                <asp:ListItem Value="jcoronadoc">Jesus Coronado Contreras</asp:ListItem>
                <asp:ListItem Value="jriosa">Jose Juan Rios AGuilar</asp:ListItem>
                <asp:ListItem Value="Lucerom">Lucero Miguel Agustin</asp:ListItem>
                <asp:ListItem Value="mmendozaq">Maria del Carmen Mendoz Quintanar</asp:ListItem>
                <asp:ListItem Value="nrodriguez">Nancy Rodriguez</asp:ListItem>
                <asp:ListItem Value="pperezg">Paris Perez Garcia</asp:ListItem>
                <asp:ListItem Value="victoriao">Victoria Osnava Jimenez</asp:ListItem>
                            </asp:DropDownList></asp:TableCell>
            <asp:TableHeaderCell>NUM CONSULTAS:</asp:TableHeaderCell>
            <asp:TableCell>
                <asp:TextBox ID="TextBox3" runat="server"  CssClass="form-control" ></asp:TextBox> </asp:TableCell>
        </asp:TableRow>
        
    </asp:Table>
    <br />

    <center>
        <asp:Button ID="Button1" runat="server" Text="DESCONTAR" CssClass="btn btn-warning" OnClick="Button1_Click1"></asp:Button>
    </center>

    </asp:Content>
