﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Data.SqlClient;
using System.Data;

namespace WebApplication2
{
    public partial class WebForm1 : System.Web.UI.Page
    {
        public String SQL = "";
        public SqlDataReader rs;
        public SqlDataReader rs2;
        public SqlConnection myConnection;
        public SqlDataReader myReader;
        public SqlConnection myConnection2;
        public SqlDataReader myReader2;

        public DataTable dt = new DataTable();

        public void datoos_llamadas(string nip)
        {
            dt.Columns.AddRange(new DataColumn[6] {
                            new DataColumn("nip_cliente", typeof(String)),
                            new DataColumn("id_Usuario", typeof(String)),
                            new DataColumn("nombre_llama", typeof(String)),
                            new DataColumn("num_consultas", typeof(String)),
                            new DataColumn("tema", typeof(String)),
                            new DataColumn("log_registro", typeof(String))
                        });

            SQL = "select* from fac0mxdb..Registro_Llamada2 where nip_cliente =" + nip +"   order by log_registro desc";
            rs = conex(SQL);
            if (rs.HasRows)
            {
                while (rs.Read())
                {
                    dt.Rows.Add(rs[1].ToString(), rs[2].ToString(), rs[3].ToString(), rs[4].ToString(), rs[6].ToString(), rs[7].ToString());
                    // Response.Write("<script LANGUAGE='JavaScript' >alert('" + rs[2].ToString() + "')</script>");
                }
            }

            libera();
            GridView1.DataSource = dt;
            GridView1.DataBind();
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                //Response.Write("<script LANGUAGE='JavaScript' >alert('asd')</script>");
                
                if (Request.QueryString["nip"] != null)
                {
                    string val1 = Request.QueryString["nip"];
                    //Response.Write("<script LANGUAGE='JavaScript' >alert('" + val1 + "')</script>");
                    datoos_llamadas(val1);
                }
                else
                {

                }
            }
            else
            {
                //Response.Write("<script LANGUAGE='JavaScript' >alert('xxx')</script>");
            }
                

        }
        public SqlDataReader conex(string qry)
        {
            //recuperamos la conexion del config
            Configuration rootWebConfig = ConfigurationManager.OpenExeConfiguration("");
            ConnectionStringSettings connString;
            //verifica que exista al menos una cadena de conexión
            if (0 < rootWebConfig.ConnectionStrings.ConnectionStrings.Count)
            {
                //lee la cadena de conexión
                connString = rootWebConfig.ConnectionStrings.ConnectionStrings["ConexionAcceso"];
                //verifica que exista la cadena de conexión especificada
                if (null != connString)
                {
                    //crea la cadena de conexión

                    //abre la conexión
                    try
                    {
                        if (myConnection == null)
                        {
                            myConnection = new SqlConnection(connString.ConnectionString);
                            myConnection.Open();
                        }
                        //ejecuta el comando
                        try
                        {
                            SqlCommand myCommand = new SqlCommand(qry, myConnection);
                            myCommand.CommandTimeout = 5000000;
                            if (myReader != null)
                            {
                                myReader.Dispose();
                            }
                            myReader = myCommand.ExecuteReader();
                        }
                        catch (Exception e)
                        {
                            throw new System.ArgumentException("conn error", e);
                        }
                    }
                    catch (Exception e)
                    {
                        throw new System.ArgumentException("db conn error 1", e);
                    }
                    //finally { myConnection.Close(); }
                    return myReader;
                }
                else
                    throw new System.ArgumentException("ConnString is null", "original");
            }
            else
                throw new System.ArgumentException("ConnStrings is empty", "original");

        }

        public SqlDataReader conex3(string qry)
        {
            //recuperamos la conexion del config
            Configuration rootWebConfig = ConfigurationManager.OpenExeConfiguration("");
            ConnectionStringSettings connString;
            //verifica que exista al menos una cadena de conexión
            if (0 < rootWebConfig.ConnectionStrings.ConnectionStrings.Count)
            {
                //lee la cadena de conexión
                connString = rootWebConfig.ConnectionStrings.ConnectionStrings["ConexionAcceso"];
                //verifica que exista la cadena de conexión especificada
                if (null != connString)
                {
                    //crea la cadena de conexión

                    //abre la conexión
                    try
                    {
                        if (myConnection2 == null)
                        {
                            myConnection2 = new SqlConnection(connString.ConnectionString);
                            myConnection2.Open();
                        }
                        //ejecuta el comando
                        try
                        {
                            SqlCommand myCommand = new SqlCommand(qry, myConnection2);
                            myCommand.CommandTimeout = 5000000;
                            if (myReader2 != null)
                            {
                                myReader2.Dispose();
                            }
                            myReader2 = myCommand.ExecuteReader();
                        }
                        catch (Exception e)
                        {
                            throw new System.ArgumentException("conn error", e);
                        }
                    }
                    catch (Exception e)
                    {
                        throw new System.ArgumentException("db conn error 1", e);
                    }
                    //finally { myConnection.Close(); }
                    return myReader2;
                }
                else
                    throw new System.ArgumentException("ConnString is null", "original");
            }
            else
                throw new System.ArgumentException("ConnStrings is empty", "original");

        }

        //escritura
        public SqlDataReader conex2(string qry)
        {
            SqlDataReader myReader = null;
            Configuration rootConfig = ConfigurationManager.OpenExeConfiguration("");
            ConnectionStringSettings connString;
            if (0 < rootConfig.ConnectionStrings.ConnectionStrings.Count)
            {
                connString = rootConfig.ConnectionStrings.ConnectionStrings["ConexionAcceso"];
                if (null != connString)
                {
                    SqlConnection myConnection2 = new SqlConnection(connString.ConnectionString);
                    try
                    {
                        myConnection2.Open();
                        try
                        {
                            SqlCommand myCommand = new SqlCommand(qry, myConnection2);
                            //myCommand.CommandTimeout = 5000;
                            myCommand.ExecuteNonQuery();
                        }
                        catch (Exception e)
                        {
                            throw new System.ArgumentException("conn error", e);
                        }
                    }
                    catch (Exception e)
                    {
                        throw new System.ArgumentException("db conn error 1", e);
                    }
                    //finally { myConnection2.Close(); }
                    return myReader;
                }
                else
                    throw new System.ArgumentException("ConnString is null", "original");
            }
            else
                throw new System.ArgumentException("ConnStrings is empty", "original");
        }

        public void libera()
        {
            if (myReader != null)
            {
                myReader.Dispose();
            }
            if (myReader2 != null)
            {
                myReader2.Dispose();
            }
            if (myConnection != null)
            {
                myConnection.Close();
            }
            if (myConnection2 != null)
            {
                myConnection2.Close();
            }

        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            if (TextBox1.Text.Length > 0)
            {
               
                GridView1.DataSource = "";
                GridView1.DataBind();
                datoos_llamadas(TextBox1.Text);

            }
            else
            {
                Response.Write("<script LANGUAGE='JavaScript' >alert('favor de colocar algun dato para buscar')</script>");
            }
        }

        protected void Page_PreRender(object sender, EventArgs e)
        {
            if (myConnection != null)
            {
                if ((int)myConnection.State == 1)
                {
                    myConnection.Close();
                }
            }

            if (myConnection2 != null)
            {
                if ((int)myConnection2.State == 1)
                {
                    myConnection2.Close();
                }
            }
        }
    }
}