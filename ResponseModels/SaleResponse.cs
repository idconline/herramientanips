﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ResponseModels
{
    public class SaleResponse
    {

        public int delivery_method_id { get; set; }
        public int partnet_invoice_id { get; set; }
        public string name { get; set; }
        public int id { get; set; }

        public override string ToString()
        {
            return $"delivery_method_id={delivery_method_id}, partnet_invoice_id={partnet_invoice_id}, name={name}, id={id}";
        }
    }
}
