﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ResponseModels
{
    public class UserResponse
    {

        public int Id_Interno { get; set; }
        public int Customer_Address_Seq { get; set; }
        public int Customer_Id { get; set; }
        public string EMail { get; set; }

        public override string ToString()
        {
            return $"Id_interno={Id_Interno}, Customer_Address_Seq={Customer_Address_Seq}, Customer_Id={Customer_Id}, EMail={EMail}";
        }
    }
}
